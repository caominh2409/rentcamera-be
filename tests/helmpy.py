from src.core.app.deploy.helmpy.chart import Chart

chart = Chart(
    name='core-engine',
    version='1.0.0',
    source=dict(
        type='oci',
        path='asia-southeast1-docker.pkg.dev/airasia-goquo-dev/ota/charts'
    )
)

chart._pull()
chart._build()
