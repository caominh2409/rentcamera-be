from pprint import pprint

from src.common.dict_utils import merge_dicts

from_dict = {
    'a_level_0': {
        'a_level_0_1': {
            'a': 1,
            'b': 2,
            'c': [1, 2, 3]
        }
    },
    'b_level_0': {
        'a': 1,
        'b': 2,
        'c': [1, 2, 3]
    },
    'c_level_0': {
        'd': [1, 2, 3, 4, 5, 6, 7]
    }
}

to_dict = {
    'a_level_0': {
        'a_level_0_1': {
            'a': 0,
            'b': 0,
            'c': [1, 2, 3, 4]
        }
    },
    'b_level_0': {
        'a': 0,
        'b': 0,
        'c': [1, 2, 3, 4, 5]
    },
    'c_level_0': {
        'c': [1, 2]
    }
}

print('ORIGIN')
pprint(to_dict)
print('MERGED')
pprint(merge_dicts(from_dict, to_dict))
