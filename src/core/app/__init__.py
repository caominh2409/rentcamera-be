import os
import uuid
import secrets

from src.bases.core import Core
# from src.models import (App, AppCurrencySettings, AppVersion,
#                         AppGeneralSettings, AppMember, AppNotificationSettings,
#                         AppBookingSettings, AppDatetimeSettings,
#                         AppAgePolicy, AppContact, AppCmsSettings,
#                         Language, Currency, Product, ShoppingForm,
#                         Supplier, CmsTemplate, TravelerField, HtmlTemplate,
#                         BookingQuestion)
from src.common.utils import get_now
from src.bases.error.core import CoreError
from src.core.process import ProcessLogic
from src.core.language import LanguageLogic
from src.core.currency import CurrencyLogic
from src.core.product import ProductLogic
from config import ROOT_PATH

class AppLogic(Core):
    pass

# class AppLogic_Test(Core):
#     setting_map = {
#         'general_settings': AppGeneralSettings,
#         'currency_settings': AppCurrencySettings,
#         'datetime_settings': AppDatetimeSettings,
#         'booking_settings': AppBookingSettings,
#         'age_policy': AppAgePolicy,
#         'cms_settings': AppCmsSettings,
#         'notification_settings': AppNotificationSettings
#     }

#     def update_settings(self, app, settings):
#         for key, model in self.setting_map.items():
#             if key not in settings:
#                 continue
#             setting_data = settings.get(key) or {}

#             st = self.session.query(model).filter(
#                 model.app_id == app.id
#             ).first()
#             if not st:
#                 st = model()
#                 st.app_id = app.id

#             st.update(**setting_data)

#             if model is AppBookingSettings:
#                 duplicate_query = self.session.query(
#                     AppBookingSettings
#                 ).filter(
#                     AppBookingSettings.code_prefix == st.code_prefix,
#                     AppBookingSettings.app_id == st.app_id,
#                 )
#                 if st.id:
#                     duplicate_query = duplicate_query.filter(
#                         AppBookingSettings.id != st.id
#                     )
#                 if duplicate_query.first():
#                     raise CoreError('DuplicateBookingCodePrefix')

#             self.session.add(st)

#     def update(self,
#                app: App,
#                account_id: str,
#                version: AppVersion = None,
#                name: str = None,
#                **kwargs
#                ) -> tuple:

#         processes = []

#         if name:
#             app.name = name

#         if version and version.id != app.version_id:
#             app.version = version
#             app.version_id = version.id

#             process_logic = ProcessLogic(self.session)

#             process = process_logic.create(
#                 type='AppDeployment',
#                 code='update',
#                 scope_id=app.id,
#                 scope_type='App',
#                 account_id=account_id
#             )
#             processes.append(process)

#         app.updated_at = get_now()

#         self.update_settings(app, kwargs)

#         self.session.add(app)

#         return app, processes

#     def update_contact(self,
#                        app: App,
#                        website: str = None,
#                        facebook: str = None,
#                        twitter: str = None,
#                        whatsapp: str = None,
#                        email: str = None,
#                        phone: str = None,
#                        ):
#         contact = self.session.query(AppContact).filter(
#             AppContact.app_id == app.id
#         ).first()
#         if not contact:
#             contact = AppContact()
#             contact.app = app

#         if website is not None:
#             contact.website = website

#         if facebook is not None:
#             contact.facebook = facebook

#         if twitter is not None:
#             contact.twitter = twitter

#         if whatsapp is not None:
#             contact.whatsapp = whatsapp

#         if email is not None:
#             contact.email = email

#         if phone is not None:
#             contact.phone = phone

#         self.session.add(contact)

#         return contact

#     def create_member(self,
#                       app: App,
#                       account_id: str,
#                       account: dict
#                       ):
#         if self.session.query(AppMember).filter(
#                 AppMember.app_id == app.id,
#                 AppMember.account_id == account_id
#         ).first():
#             raise CoreError('InvalidParams', 'Duplicate member')

#         result = AppMember()
#         result.account_id = account_id
#         result.account = account
#         result.app = app

#         self.session.add(result)

#         return result

#     def delete_member(self,
#                       member: AppMember,
#                       ):
#         self.session.add(member)
#         return member

#     def create(self,
#                name: str,
#                code: str,
#                settings: dict,
#                version: str = None,
#                languages: list = None,
#                currencies: list = None,
#                switches: list = None,
#                products: list = None,
#                traveler_fields: list = None,
#                ):
#         if self.session.query(App).filter(
#                 App.code == code
#         ).first():
#             raise CoreError('InvalidParams', 'Duplicate code')

#         app = App()
#         app.id = str(uuid.uuid4())
#         app.secret_key = self._generate_secret_key()
#         app.code = code
#         app.name = name
#         app.status = 'non-active'

#         latest_app = self.session.query(App).order_by(
#             App.number.desc()
#         ).first()
#         if latest_app:
#             number = latest_app.number + 1
#         else:
#             number = 1
#         app.number = number

#         if version:
#             app_version = self.session.query(AppVersion).filter(
#                 AppVersion.code == version
#             ).first()
#             if not app_version:
#                 raise CoreError('InvalidParams', 'Version not found')
#         else:
#             app_version = self.session.query(AppVersion).order_by(
#                 AppVersion.code.desc()
#             ).first()
#         app.version = app_version

#         if not languages:
#             languages = [{'code': 'en-US', 'is_default': True}]

#         language_logic = LanguageLogic(self.session)

#         for lang in languages:
#             lang_code = lang['code']
#             system_language = self.session.query(Language).filter(
#                 Language.code == lang_code,
#                 Language.scope_type == 'System',
#             ).first()
#             if not system_language:
#                 raise CoreError('InvalidParams',
#                                 f'Unsupported language {lang_code}')
#             language_logic.create(
#                 name=system_language.name,
#                 code=lang_code,
#                 scope_type='App',
#                 scope_id=app.id,
#                 data=system_language.data,
#                 is_default=lang['is_default'],
#                 avatar=system_language.avatar
#             )

#         if not currencies:
#             currencies = [{'code': 'USD', 'is_default': True}]

#         currency_logic = CurrencyLogic(self.session)

#         for cur in currencies:
#             cur_code = cur['code']
#             system_currency = self.session.query(Currency).filter(
#                 Currency.code == cur_code,
#                 Currency.scope_type == 'System',
#             ).first()
#             if not system_currency:
#                 raise CoreError('InvalidParams',
#                                 f'Unsupported currency {cur_code}')
#             currency_logic.create(
#                 name=system_currency.name,
#                 country_name=system_currency.country_name,
#                 rounding=system_currency.rounding,
#                 symbol=system_currency.symbol,
#                 code=cur_code,
#                 scope_type='App',
#                 scope_id=app.id,
#                 is_default=cur['is_default'],
#             )

#         if switches:
#             supplier_logic = SupplierLogic(self.session)
#             for sw in switches:
#                 supplier = self.session.query(Supplier).get(sw['supplier_id'])
#                 if not supplier:
#                     raise CoreError('InvalidParams', 'Supplier not found')
#                 supplier_logic.create_switch(
#                     app=app,
#                     supplier=supplier,
#                     credentials=sw['credentials'],
#                     name=sw['name'],
#                 )

#         if products:
#             product_logic = ProductLogic(self.session)
#             for pr in products:
#                 pr_code = pr['code']
#                 system_product = self.session.query(Product).filter(
#                     Product.code == pr_code,
#                     Product.scope_type == 'System'
#                 ).first()
#                 if not system_product:
#                     raise CoreError('InvalidParams',
#                                     f'Unsupported product {pr_code}')
#                 product_logic.create(
#                     scope_type='App',
#                     scope_id=app.id,
#                     name=system_product.name,
#                     icon=system_product.icon,
#                     code=system_product.code,
#                 )

#         if traveler_fields is None:
#             traveler_fields = []
#             system_products = self.session.query(Product).filter(
#                 Product.scope_type == 'System'
#             ).all()
#             for tf in self.session.query(TravelerField).filter(
#                     TravelerField.scope_type == 'System'
#             ):
#                 traveler_fields.append({
#                     'key': tf.key,
#                     'conditions': [
#                         {
#                             'product_code': sp.code,
#                             'enabled': True,
#                             'route_type': 'all'
#                         } for sp in system_products
#                     ]
#                 })

#         traveler_logic = TravelerLogic(session=self.session)
#         for tf in traveler_fields:
#             key = tf['key']

#             field = self.session.query(TravelerField).filter(
#                 TravelerField.scope_type == 'System',
#                 TravelerField.key == key
#             ).first()
#             if not field:
#                 raise CoreError('InvalidParams',
#                                 'Traveler field not found')
#             traveler_logic.create_field(
#                 scope_id=app.id,
#                 scope_type='App',
#                 key=key,
#                 translation_label=field.translation_label,
#                 data_type=field.data_type,
#                 placeholder=field.placeholder,
#                 icon=field.icon,
#                 conditions=tf.get('conditions', []),
#             )

#         cms_logic = CmsLogic(self.session)

#         system_site_template = self.session.query(CmsTemplate).filter(
#             CmsTemplate.scope_type == 'System'
#         ).first()
#         site_template = cms_logic.clone_template(system_site_template)
#         site_template.scope_type = 'App'
#         site_template.scope_id = app.id
#         site_template.enabled = True
#         self.session.add(site_template)

#         for p in self.session.query(Product).filter(
#                 Product.scope_type == 'System'
#         ):
#             sf = ShoppingForm()
#             sf.app_id = app.id
#             sf.combine = p.code
#             sf.label = {'en-US': p.name}
#             sf.icon = p.icon
#             self.session.add(sf)

#         self.create_default_html_templates(app)
#         self.update_settings(app, settings)

#         self.session.add(app)

#         return app

#     def _generate_secret_key(self, num_bytes=8):
#         result = secrets.token_hex(num_bytes)
#         duplication = self.session.query(App).filter(
#             App.secret_key == result
#         ).first()
#         if duplication:
#             return self._generate_secret_key(num_bytes)
#         return result

#     def create_default_html_templates(self, app):
#         template_type_maps = {
#             'BookingMail': {
#                 'names': ['confirm', 'cancel', 'in-progress'],
#                 'dir': os.path.join(
#                     ROOT_PATH,
#                     f'static/bookings/mailing/default'
#                 )
#             },
#             'BookingPDF': {
#                 'names': ['flight', 'tour', 'hotel'],
#                 'dir': os.path.join(
#                     ROOT_PATH,
#                     f'static/bookings/pdf/default'
#                 )
#             }
#         }
#         scope_type = 'App'

#         for template_type, template_data in template_type_maps.items():
#             for t in template_data['names']:
#                 if self.session.query(HtmlTemplate).filter(
#                         HtmlTemplate.name == t,
#                         HtmlTemplate.type == template_type,
#                         HtmlTemplate.scope_id == app.id,
#                         HtmlTemplate.scope_type == scope_type,
#                         HtmlTemplate.parent_id.is_(None)
#                 ).first():
#                     continue

#                 template = HtmlTemplate()
#                 template.name = t
#                 template.type = template_type
#                 template.scope_id = app.id
#                 template.scope_type = scope_type
#                 template.id = str(uuid.uuid4())
#                 template.deletable = False

#                 template_dir = os.path.join(
#                     template_data['dir'],
#                     t
#                 )
#                 with open(os.path.join(template_dir,
#                                        'index.html'), 'r') as file_ref:
#                     template.content = file_ref.read()
#                 self.session.add(template)

#                 children_dir = os.path.join(template_dir,
#                                             'children')
#                 for child_dir in os.listdir(children_dir):
#                     child = HtmlTemplate()
#                     child.parent_id = template.id
#                     child.scope_type = scope_type
#                     child.scope_id = app.id
#                     child.name = child_dir
#                     child.type = template_type
#                     child.deletable = False
#                     with open(os.path.join(
#                             children_dir,
#                             f'{child_dir}/index.html'
#                     ), 'r') as file_ref:
#                         child.content = file_ref.read()
#                     self.session.add(child)

#     def init_booking_questions(self, app):
#         result = []
#         for q in self.session.query(BookingQuestion).filter(
#                 BookingQuestion.scope_type == 'System'
#         ):

#             if self.session.query(BookingQuestion).filter(
#                     BookingQuestion.scope_type == App.__name__,
#                     BookingQuestion.scope_id == app.id,
#                     BookingQuestion.key == q.key
#             ).first():
#                 continue
#             question = BookingQuestion()
#             question.scope_type = App.__name__
#             question.scope_id = app.id
#             question.key = q.key
#             question.update(**q.to_json(
#                 exclude_fields=['id', 'created_at', 'updated_at',
#                                 'scope_type', 'scope_id']
#             ))

#             result.append(question)
#             self.session.add(question)
#         return result
