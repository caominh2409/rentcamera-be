import semantic_version

from src.bases.core import Core
from src.models.application import AppVersion
from src.models.docker_image import DockerImage
from src.bases.error.core import CoreError


class AppVersionLogic(Core):
    def create(self,
               description: str,
               code: str = None,
               frontend_version: str = None,
               backend_version: str = None,
               meta: dict = None,
               ):
        result = AppVersion()
        result.description = description

        if meta:
            result.meta = meta

        if code:
            try:
                sv = semantic_version.Version(code)
            except ValueError as e:
                raise CoreError('InvalidParas', str(e))

        else:
            last_version = self.session.query(AppVersion).order_by(
                AppVersion.created_at.desc()
            ).first()

            last_code = None
            if last_version:
                last_code = last_version.code
            last_sv = semantic_version.Version(last_code)
            sv = last_sv.next_minor()

        result.code = str(sv)

        images = self.session.query(DockerImage)
        if not frontend_version:
            latest_frontend_image = images.filter(
                DockerImage.type == 'frontend'
            ).order_by(DockerImage.created_at.desc()).first()
            frontend_version = latest_frontend_image.version

        if not backend_version:
            latest_backend_image = images.filter(
                DockerImage.type == 'backend'
            ).order_by(DockerImage.created_at.desc()).first()
            backend_version = latest_backend_image.version

        result.frontend_version = frontend_version
        result.backend_version = backend_version

        self.session.add(result)

        return result

    def update(self,
               version: AppVersion,
               description: str = None,
               backend_version: str = None,
               frontend_version: str = None,
               meta: dict = None
               ):

        if description is not None:
            version.description = description

        if backend_version:
            version.backend_version = backend_version

        if frontend_version:
            version.frontend_version = frontend_version

        if meta:
            version.meta = meta

        self.session.add(version)
        return version
