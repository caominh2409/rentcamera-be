from pymongo import ASCENDING, DESCENDING

from src.bases.core import Core


class DatabaseHandler(Core):
    coll_default_indexes = {
        'id': {
            'type': ASCENDING
        },
        'created_at': {
            'type': DESCENDING
        },
        'updated_at': {
            'type': DESCENDING
        }
    }
    models = {
        'app': {
            'indexes': {}
        },
        'product': {
            'indexes': {
                'code': {},
                'gateway_id': {},
            }
        },
        'shopping': {
            'indexes': {
                'expired_at': {},
                'cleaned': {}
            }
        },
        'shopping_price': {
            'indexes': {
                'shopping_id': {},
                'product_code': {}
            }
        },
        'search_request': {
            'indexes': {
                'shopping_id': {}
            }
        },
        'storage_cache': {
            'indexes': {
                'shopping_id': {}
            }
        },
        'search_result': {
            'indexes': {
                'search_request_id': {},
                'shopping_id': {},
                'product_code': {},
                'data.id': {},
                'data.price_info.total_amount': {},

                # car_rental
                'data.info.name': {},
                'data.supplier.location_type': {},
                'data.supplier.name': {},
                'data.amenity.fuel_policy.value': {},
                'data.amenity.auto_transmission.value': {},
                'data.amenity.unlimited_mileage.value': {},
                'data.amenity.doors.value': {},
                'data.amenity.air_conditioner.value': {},

                # flight
                'data.fare.total_amount': {},
                'data.leave_duration': {},
                'data.return_duration': {},
                'data.leave_id': {},
                'data.return_id': {},
                'data.route_type': {},
                'data.departure_airport_code': {},
                'data.arrival_airport_codes': {},
                'data.leave_stops': {},
                'data.return_stops': {},
                'data.departure_hour': {},
                'data.return_hour': {},
                'data.return_datetime': {},
                'data.departure_datetime': {},
                'data.segments.airline.name': {},
                'data.segments.airline_code': {},
                'data.segments.type': {},

                # hotel
                'data.name': {},
                'data.star': {},
                'data.location.country_code': {},
                'data.location.point': {
                    'type': '2dsphere'
                },
                'data.amenities': {},
                'data.ranking': {},
                'data.ratings.tripadvisor.review_count': {
                    'type': DESCENDING
                },
                'data.ratings.tripadvisor.rating_value': {
                    'type': DESCENDING
                },

                # tour
                'data.info.categories': {},
                'data.viator_order': {},

                # tour_package
                'data.title': {}
            }
        },
        'cart_item': {
            'indexes': {
                'shopping_id': {},
                'product_code': {},
            }
        },
        'payment': {
            'indexes': {
                'transaction_id': {},
                'shopping_id': {},
                'handled': {},
            }
        },
    }

    def __init__(self, session, mongo, app):
        super(DatabaseHandler, self).__init__(session=session)
        self.app = app
        self.mongo = mongo
        self.db = self.mongo.get_database(app.release_name)

    def create_indexes(self):

        for coll_name, coll_data in self.models.items():
            coll = self.db[coll_name]
            indexes = coll_data.get('indexes') or {}
            for default_idx, options in self.coll_default_indexes.items():
                if default_idx not in indexes:
                    indexes[default_idx] = options

            for idx, options in indexes.items():
                idx_type = options.get('type', ASCENDING)
                coll.create_index([(idx, idx_type)])

    def initialize(self):
        self.create_indexes()
