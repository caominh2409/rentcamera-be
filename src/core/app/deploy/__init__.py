from gevent.pool import Pool

from src.bases.core import Core
from src.bases.error.core import CoreError
from src.common.utils import get_now
# from src.models import Process

from .helmpy import Chart
from .jobs import (
    InstallingApp, InitializingDb,
    HealthCheckingApp, UninstallingApp,
    SyncingDb, AuthenticatingGcp
)


class Deployer(Core):
    def __init__(self,
                 session,
                 redis,
                 mongo,
                 process,
                 app,
                 ):
        super(Deployer, self).__init__(session=session)

        self.app = app
        self.process = process
        self.redis = redis
        self.mongo = mongo
        self.jobs = []

    def run(self):
        worker_pool = Pool(50)

        if self.process.code == 'start':
            final_status = 'running'
            self.jobs = [
                {
                    'handler': InitializingDb,
                    'async': True,
                    'status': 'pending',
                },
                {
                    'handler': SyncingDb,
                    'async': True,
                    'status': 'pending',
                },
                {
                    'handler': AuthenticatingGcp,
                    'async': False,
                    'status': 'pending',
                },
                {
                    'handler': InstallingApp,
                    'async': False,
                    'status': 'pending',
                },
                {
                    'handler': HealthCheckingApp,
                    'status': 'pending',
                    'async': False
                }
            ]

        elif self.process.code == 'stop':
            final_status = 'non-active'
            self.jobs = [
                {
                    'handler': AuthenticatingGcp,
                    'async': False,
                    'status': 'pending',
                },
                {
                    'handler': UninstallingApp,
                    'async': False,
                    'status': 'pending',
                }
            ]
        elif self.process.code == 'update':
            final_status = self.app.status
            self.jobs = [
                {
                    'handler': InitializingDb,
                    'status': 'pending',
                    'async': True
                },
                {
                    'handler': SyncingDb,
                    'status': 'pending',
                    'async': True
                },
                {
                    'handler': AuthenticatingGcp,
                    'async': False,
                    'status': 'pending',
                },
                {
                    'handler': InstallingApp,
                    'status': 'pending',
                    'async': False
                }
            ]

        else:
            raise CoreError('InvalidParams', 'Unsupported code')

        self._cache_process()

        for job in self.jobs:
            if job.get('async'):
                worker_pool.spawn(
                    self._handle_job,
                    job
                )
            else:
                self._handle_job(job)
                if job.get('error'):
                    break

        worker_pool.join()

        failed_jobs = list(filter(
            lambda j: j['status'] == 'failed',
            self.jobs
        ))
        if failed_jobs:
            self.process.status = 'failed'
            self.app.status = 'error'
        else:
            self.process.status = 'done'
            self.app.status = final_status

        self._cache_process()

        self.process.jobs = list(map(
            lambda j: dict(
                name=j['handler'].__name__,
                status=j['status'],
                error=j.get('error')
            ),
            self.jobs
        ))
        self.process.finished_at = get_now()

        self.session.add(self.process)
        self.session.add(self.app)
        self.session.commit()

    def _handle_job(self, job: dict):
        kwargs = job.get('kwargs') or {}
        handler = job['handler'](
            session=self.session,
            app=self.app,
            mongo=self.mongo,
            redis=self.redis
        )
        try:
            ok, error = handler.run(**kwargs)
        except Exception as e:
            ok = False
            error = str(e)
        if ok:
            job['status'] = 'success'
        else:
            job['status'] = 'failed'
            job['error'] = error
        self._cache_process()

    def _cache_process(self):
        pass
        # cache_key = '{model_name}:{app_id}'.format(
        #     model_name=Process.__name__,
        #     app_id=self.app.id
        # )
        # cache_data = self.process.to_json()
        # cache_data['jobs'] = list(map(
        #     lambda j: dict(
        #         name=j['handler'].__name__,
        #         status=j['status'],
        #         error=j.get('error')
        #     ),
        #     self.jobs
        # ))

        # self.redis.set(cache_key, cache_data)
