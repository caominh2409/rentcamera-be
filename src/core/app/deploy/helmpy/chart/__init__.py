import os
import shutil
import yaml

from src.common.utils import execute_command
from src.common.constants import HELM_CHARTS_DIR, TMP_DIR
from src.common.dict_utils import merge_dicts


class Chart(object):

    def __init__(self,
                 name: str,
                 version: str,
                 source: dict,
                 ):
        self.name = name
        self.version = version
        self.source = source
        self._pull()

    @property
    def _chart_dir(self):
        return os.path.join(HELM_CHARTS_DIR, self.name)

    def _build(self, values: dict = None):
        values_file = os.path.join(self._chart_dir, 'values.yaml')

        if os.path.exists(values_file):
            with open(values_file, 'r') as r_file:
                origin_values = yaml.safe_load(r_file)
        else:
            origin_values = dict()

        final_values = merge_dicts(
            from_dict=values or {},
            to_dict=origin_values
        )

        with open(values_file, 'w') as file_ref:
            file_ref.write(yaml.dump(final_values, default_flow_style=False))

        command = [
            'helm package',
            self._chart_dir,
            f'--destination={TMP_DIR}'
        ]

        result_file = f'{self.name}-{self.version}.tgz'
        result_file = os.path.join(TMP_DIR, result_file)

        execute_command(command)

        return result_file

    def _pull(self):
        command = ['helm pull']

        path = self.source.get('location')

        oci_url = f'oci://{path}/{self.name}'

        command.extend([
            oci_url,
            f'--version={self.version}',
            '--untar',
            f'--destination={HELM_CHARTS_DIR}'
        ])

        if os.path.exists(self._chart_dir):
            shutil.rmtree(self._chart_dir)

        return execute_command(command)

    def upgrade(self,
                release_name: str,
                namespace: str,
                values: dict = None,
                install: bool = False
                ):

        chart_file = self._build(values=values)

        command = [
            f'helm upgrade {release_name}',
            chart_file,
            f'--namespace={namespace}',
            '--create-namespace',
            '--cleanup-on-fail',
        ]

        if install:
            command.append('--install')

        output = execute_command(command)

        os.remove(chart_file)

        return output

    def uninstall(self, release_name: str, namespace: str):
        command = [
            f'helm uninstall {release_name}',
            f'--namespace={namespace}',
        ]
        output = execute_command(command)
        return output
