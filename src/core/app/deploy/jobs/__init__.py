import os
import time
import requests
import base64

from src.core.app.database import DatabaseHandler
from src.common.dict_utils import merge_dicts
from src.common.utils import execute_command
from config import (SERVICES, ENVIRONMENT, REDIS, IMAGE_REGISTRY,
                    GCP_PROJECT, BASE_DOMAIN, ROOT_PATH,
                    MONGO_URI, BACKEND_IMAGE_REPO,
                    GCP_K8S_CLUSTER, GCP_K8S_CLUSTER_ZONE,
                    WEBAPP_IMAGE_REPO, HELM_CHART_REPO)

from ..helmpy.chart import Chart


class DeploymentJob(object):
    def __init__(self, session, app, mongo, redis):
        self.session = session
        self.app = app
        self.mongo = mongo
        self.redis = redis

        self.status = 'pending'
        self.error_code = None
        self.error_message = None

    def run(self, *args, **kwargs):
        raise NotImplementedError


class InitializingDb(DeploymentJob):
    def run(self):
        db_handler = DatabaseHandler(
            app=self.app,
            session=self.session,
            mongo=self.mongo
        )
        try:
            db_handler.initialize()
        except Exception as e:
            return False, str(e)

        return True, 'ok'


class SyncingDb(DeploymentJob):
    def run(self):
        pass
        # logic = AppSyncLogic(mongo=self.mongo,
        #                      session=self.session,
        #                      app=self.app)

        # try:
        #     logic.sync_all()
        # except Exception as e:
        #     return False, str(e)

        return True, 'ok'


class InstallingApp(DeploymentJob):
    def gen_chart_values(self) -> dict:
        version = self.app.version
        backend_version = version.backend_version
        frontend_version = version.frontend_version

        domains = [self.app.default_domain]

        custom_domains = getattr(self.app, 'custom_domains', [])
        if custom_domains:
            domains.extend(list(map(
                lambda cd: cd.value,
                custom_domains
            )))

        backend_image_repo = '{image_registry}/{project}/{image_repo}'.format(
            image_registry=IMAGE_REGISTRY,
            project=GCP_PROJECT,
            image_repo=BACKEND_IMAGE_REPO
        )
        webapp_image_repo = '{image_registry}/{project}/{image_repo}'.format(
            image_registry=IMAGE_REGISTRY,
            project=GCP_PROJECT,
            image_repo=WEBAPP_IMAGE_REPO
        )

        core_url = f'https://{BASE_DOMAIN}/api/v1'

        result = {
            'environment': {
                'APP_ID': self.app.id,
                'APP_CODE': self.app.code,

                'SECRET_KEY': self.app.secret_key,

                'ENV': ENVIRONMENT,

                'DEFAULT_DOMAIN': self.app.default_domain,

                'REDIS_HOST': REDIS['host'],
                'REDIS_PORT': REDIS['port'],
                'REDIS_PASSWORD': REDIS['password'],
                'REDIS_DB': self.app.number,

                'MONGO_URI': f'{MONGO_URI}',

                'CORE_URL': core_url,
                'STORAGE_URL': SERVICES['Storage']['url'],
                'IAM_URL': SERVICES['IAM']['url'],

                'API_URL': f'https://{self.app.default_domain}/api',

            },
            'domains': domains,
            'images': {
                'backend': {
                    'repository': backend_image_repo,
                    'tag': backend_version
                },
                'webapp': {
                    'repository': webapp_image_repo,
                    'tag': frontend_version
                },
            }
        }

        return result

    def run(self, values: dict = None):
        pass
        # latest_chart = self.session.query(HelmChart).order_by(
        #     HelmChart.created_at.desc()
        # ).first()

        # chart_repo = '{image_registry}/{project}/{repo}'.format(
        #     image_registry=IMAGE_REGISTRY,
        #     project=GCP_PROJECT,
        #     repo=HELM_CHART_REPO
        # )

        # chart = Chart(
        #     name=latest_chart.name,
        #     version=latest_chart.version,
        #     source=dict(
        #         type='oci',
        #         location=chart_repo
        #     )
        # )

        # final_values = self.gen_chart_values()
        # if values:
        #     final_values = merge_dicts(values, final_values)

        # try:
        #     chart.upgrade(
        #         release_name=self.app.release_name,
        #         namespace=self.app.release_name,
        #         values=final_values,
        #         install=True
        #     )

        # except Exception as e:
        #     return False, str(e)

        return True, 'ok'


class UninstallingApp(DeploymentJob):
    def run(self):
        latest_chart = self.session.query(HelmChart).order_by(
            HelmChart.created_at.desc()
        ).first()

        chart_repo = '{image_registry}/{project}/{repo}'.format(
            image_registry=IMAGE_REGISTRY,
            project=GCP_PROJECT,
            repo=HELM_CHART_REPO
        )

        chart = Chart(
            name=latest_chart.name,
            version=latest_chart.version,
            source=dict(
                type='oci',
                location=chart_repo
            )
        )
        try:
            chart.uninstall(
                release_name=self.app.release_name,
                namespace=self.app.release_name
            )

        except Exception as e:
            return False, str(e)

        return True, 'ok'


class HealthCheckingApp(DeploymentJob):
    REQUEST_TIMEOUT = 10
    MAX_RETRY = 10
    RETRY_INTERVAL = 5

    @property
    def base_url(self):
        return f'https://{self.app.default_domain}'

    def _healthcheck_webapp(self):
        try:
            res = requests.get(self.base_url, timeout=self.REQUEST_TIMEOUT)
        except Exception as e:
            return False

        if res.status_code != 200:
            return False

        return True

    def _healthcheck_backend(self):
        url = f'{self.base_url}/api/health-check'
        try:
            res = requests.get(url, timeout=self.REQUEST_TIMEOUT)
        except Exception as e:
            return False

        if res.status_code != 200:
            return False

        try:
            data = res.json()
        except Exception as e:
            return False

        if not data.get('ok'):
            return False

        return True

    def run(self):
        for i in range(self.MAX_RETRY):
            check_result = True

            for func in [self._healthcheck_webapp, self._healthcheck_backend]:
                if func():
                    continue
                check_result = False

            if check_result:
                return True, 'ok'

            time.sleep(i ** 2)

        return False, 'HealthCheckFailed'


class AuthenticatingGcp(DeploymentJob):
    def run(self, *args, **kwargs):
        credentials_file = os.path.join(ROOT_PATH, 'gcp.credentials.json')

        # gcp login
        command = [
            'gcloud auth activate-service-account',
            f'--key-file={credentials_file}'
        ]
        execute_command(command)
        command = [
            'gcloud container clusters get-credentials',
            GCP_K8S_CLUSTER,
            f'--zone={GCP_K8S_CLUSTER_ZONE}',
            f'--project={GCP_PROJECT}'
        ]
        execute_command(command)

        # helm login
        command = [
            'helm', 'registry', 'login', f'https://{IMAGE_REGISTRY}',
            '-u', '_json_key_base64'
        ]

        with open(credentials_file, 'rb') as file_ref:
            password = base64.b64encode(file_ref.read()).decode()
        command.extend([
            '--password',
            password
        ])
        execute_command(command)

        return True, 'ok'
