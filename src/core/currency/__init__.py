from sqlalchemy import or_

from src.bases.core import Core
from src.bases.error.core import CoreError

from .crawler import ConversionCrawler

from src.common.constants import ROUNDING_DEFAULT, CONVERSION_RATE_DEFAULT
class CurrencyLogic(Core):
    pass
    # def create(self,
    #            name: str,
    #            code: str,
    #            scope_type: str,
    #            country_name: str = None,
    #            symbol: str = None,
    #            rounding: int = None,
    #            scope_id: str = None,
    #            is_default: bool = None
    #            ):

    #     result = Currency(
    #         name=name,
    #         code=code,
    #         scope_type=scope_type,
    #         scope_id=scope_id
    #     )
    #     if symbol:
    #         result.symbol = symbol

    #     if country_name:
    #         result.country_name = country_name

    #     if rounding is not None:
    #         result.rounding = rounding

    #     self.validate_currency(result)

    #     if is_default:
    #         self.set_default_currency(result)

    #     self.session.add(result)

    #     if result.scope_type == 'App':
    #         # Cloning conversions from system scope
    #         for cc in self.session.query(CurrencyConversion).filter(
    #             CurrencyConversion.scope_type == 'System',
    #             or_(
    #                 CurrencyConversion.from_code == result.code,
    #                 CurrencyConversion.to_code == result.code
    #             )
    #         ):
    #             if self.session.query(CurrencyConversion).filter(
    #                 CurrencyConversion.scope_id == scope_id,
    #                 CurrencyConversion.scope_type == scope_type,
    #                 CurrencyConversion.from_code == cc.from_code,
    #                 CurrencyConversion.to_code == cc.to_code
    #             ).first():
    #                 continue
    #             self.create_conversion(
    #                 from_code=cc.from_code,
    #                 to_code=cc.to_code,
    #                 rate=cc.rate,
    #                 scope_type='App',
    #                 scope_id=result.scope_id
    #             )

    #     return result

    # def update(self,
    #            currency: Currency,
    #            code: str = None,
    #            name: str = None,
    #            rounding: int = None,
    #            symbol: str = None,
    #            country_name: str = None,
    #            is_default: bool = None,
    #            enabled: bool = None
    #            ):

    #     if name:
    #         currency.name = name

    #     if code:
    #         currency.code = code

    #     if symbol:
    #         currency.symbol = symbol

    #     if country_name:
    #         currency.country_name = country_name

    #     if rounding is not None:
    #         currency.rounding = rounding

    #     if enabled is not None:
    #         currency.enabled = enabled

    #     self.validate_currency(currency)

    #     if is_default:
    #         self.set_default_currency(currency)

    #     self.session.add(currency)

    #     return currency

    # def set_default_currency(self, currency: Currency):
    #     others = self.session.query(Currency).filter(
    #         Currency.scope_type == currency.scope_type,
    #         Currency.code != currency.code,
    #         Currency.is_default.is_(True)
    #     )
    #     if currency.scope_id:
    #         others = others.filter(
    #             Currency.scope_id == currency.scope_id
    #         )
    #     if currency.id:
    #         others = others.filter(
    #             Currency.id != currency.id
    #         )
    #     for c in others:
    #         c.is_default = False
    #         self.session.add(c)
    #     currency.is_default = True
    #     self.session.add(currency)

    # def validate_currency(self, currency: Currency):
    #     query = self.session.query(Currency).filter(
    #         Currency.code == currency.code,
    #         Currency.scope_type == currency.scope_type
    #     )
    #     if currency.scope_id:
    #         query = query.filter(
    #             Currency.scope_id == currency.scope_id
    #         )
    #     if currency.id:
    #         query = query.filter(
    #             Currency.id != currency.id
    #         )
    #     if query.first():
    #         raise CoreError('DuplicateCurrency')

    # def update_conversions(self,
    #                        scope_type: str,
    #                        scope_id: str = None,
    #                        currency_codes: list = None, ):

    #     currencies = self.session.query(Currency.code)
    #     if currency_codes:
    #         currencies = currencies.filter(Currency.code.in_(currency_codes))
    #     currencies = currencies.all()

    #     conversion_crawler = ConversionCrawler()

    #     today_rates = conversion_crawler.run(
    #         codes=list(map(
    #             lambda c: c.code,
    #             currencies
    #         )),
    #         # sources=['goquo']
    #     )

    #     conversions = []

    #     for pair_name, rate in today_rates.items():
    #         from_code, to_code = pair_name.split('-')
    #         filter_params = [
    #             CurrencyConversion.from_code == from_code,
    #             CurrencyConversion.to_code == to_code,
    #             CurrencyConversion.scope_type == scope_type,
    #         ]
    #         if scope_id:
    #             filter_params.append(
    #                 CurrencyConversion.scope_id == scope_id,
    #             )
    #         conversion = self.session.query(CurrencyConversion).filter(
    #             *filter_params
    #         ).first()
    #         if conversion:
    #             conversion = self.update_conversion(
    #                 conversion=conversion,
    #                 rate=rate
    #             )
    #         else:
    #             conversion = self.create_conversion(
    #                 from_code=from_code,
    #                 to_code=to_code,
    #                 rate=rate,
    #                 scope_type=scope_type,
    #                 scope_id=scope_id
    #             )

    #         conversions.append(conversion)

    #     return conversions

    # def create_conversion(self,
    #                       from_code: str,
    #                       to_code: str,
    #                       scope_type: str,
    #                       rate: float,
    #                       scope_id: str = None):

    #     result = CurrencyConversion()
    #     result.from_code = from_code
    #     result.to_code = to_code
    #     result.rate = rate
    #     result.scope_type = scope_type

    #     if scope_id:
    #         result.scope_id = scope_id

    #     self.validate_conversion(result)

    #     self.session.add(result)

    #     return result

    # def update_conversion(self,
    #                       conversion: CurrencyConversion,
    #                       from_code: str = None,
    #                       to_code: str = None,
    #                       rate: float = None,
    #                       ):

    #     if rate is not None:
    #         conversion.rate = rate

    #     if from_code:
    #         conversion.from_code = from_code

    #     if to_code:
    #         conversion.to_code = to_code

    #     self.validate_conversion(conversion)

    #     self.session.add(conversion)

    #     return conversion

    # def validate_conversion(self, conversion: CurrencyConversion):
    #     query = self.session.query(CurrencyConversion)

    #     duplicate_query = query.filter(
    #         CurrencyConversion.from_code == conversion.from_code,
    #         CurrencyConversion.to_code == conversion.to_code,
    #         CurrencyConversion.scope_type == conversion.scope_type,
    #     )
    #     if conversion.scope_id:
    #         duplicate_query = duplicate_query.filter(
    #             CurrencyConversion.scope_id == conversion.scope_id
    #         )
    #     if conversion.id:
    #         duplicate_query = duplicate_query.filter(
    #             CurrencyConversion.id != conversion.id
    #         )

    #     if duplicate_query.first():
    #         raise CoreError('InvalidParams', 'Duplicate conversion')

    # def delete_conversion(self, conversion: CurrencyConversion):
    #     pass

    # def delete(self, currency: Currency):
    #     self.session.delete(currency)
    #     return currency

    # def currency_rounding(self, currency_code):
    #     currency_info = self.session.query(Currency).filter(Currency.code == currency_code).first()
    #     if currency_info:
    #         return currency_info.rounding
    #     else:
    #         return ROUNDING_DEFAULT
    
    # def currency_conversion_rate(self, from_code, to_code):
    #     conversion_info = self.session.query(CurrencyConversion).filter(
    #         CurrencyConversion.from_code == from_code, CurrencyConversion.to_code == to_code).first()
    #     if conversion_info:
    #         return conversion_info.rate
    #     else:
    #         return CONVERSION_RATE_DEFAULT