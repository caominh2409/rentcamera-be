from src.bases.request_handler import RequestHandler


class ConversionCrawler(RequestHandler):
    def run(self, codes: list):

        result = {}

        for code in codes:
            url = f'https://api.asgard.goquo.com/currency-rates/{code}'
            try:
                response = self._do_request(url=url, method='get')
                data = response.json().get('quotes') or {}

            except Exception as e:
                continue

            for pair, rate in data.items():
                from_code = pair[:3]
                to_code = pair[3:]
                result[f'{from_code}-{to_code}'] = rate

        return result
