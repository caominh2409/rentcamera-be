from src.bases.core import Core
from src.bases.error.core import CoreError
from src.models import File
from src.clients.google_cloud_api import GoogleCloudApi
from src.common.utils import get_now

from config import ENVIRONMENT


class FileLogic(Core):
    @staticmethod
    def _make_gs_path(file_name: str,
                      scope_type: str,
                      file_type: str = None,
                      scope_id: str = None):

        file_name = '{}__{}'.format(
            get_now(timestamp=True),
            file_name
        )

        path_info = [ENVIRONMENT]

        if file_type:
            path_info.append(file_type)

        if scope_type:
            path_info.append(scope_type)

        if scope_id:
            path_info.append(scope_id)

        path_info.append(file_name)

        return '/'.join(path_info)

    def create_file(self,
                    local_path: str,
                    content_type: str,
                    file_name: str,
                    scope_type: str,
                    type: str = None,
                    scope_id: str = None,
                    is_default: bool = None,
                    meta: dict = None) -> File:

        file = File()

        file.type = type
        file.content_type = content_type
        file.name = file_name
        file.scope_type = scope_type

        if scope_id:
            file.scope_id = scope_id

        if is_default:
            file.is_default = is_default

        if meta:
            file.meta = meta

        file.path = self._make_gs_path(scope_type=scope_type,
                                       scope_id=scope_id,
                                       file_type=type,
                                       file_name=file_name)

        gca = GoogleCloudApi()
        try:
            url = gca.upload_file(
                local_file_path=local_path,
                gs_file_path=file.path
            )
        except Exception as e:
            raise CoreError(str(e.__class__))

        file.url = url

        self.session.add(file)

        return file

    def update_file(self,
                    file: File,
                    ):

        pass

    def delete_file(self, file):
        self.session.delete(file)
        return file
