import secrets

from src.models import Account
from src.bases.core import Core
from src.bases.error.core import CoreError


class AccountLogic(Core):
    def create_account(self,
                       email: str,
                       type: str = None,
                       phone: str = None,
                       first_name: str = None,
                       last_name: str = None,
                       secret_key: str = None,
                       avatar: dict = None,
                       status: str = None,
                       ) -> Account:

        if self.session.query(Account).filter(
                Account.email == email
        ).first():
            raise CoreError('InvalidParams', 'Duplicate email')

        if type is None:
            type = 'User'

        if status is None:
            status = 'active'

        account = Account()
        account.email = email
        account.type = type
        account.phone = phone
        account.first_name = first_name
        account.last_name = last_name
        account.status = status

        if avatar:
            account.avatar = avatar

        if not secret_key:
            secret_key = secrets.token_hex(24)
        account.secret_key = account.encrypt_secret_key(secret_key)

        self.session.add(account)

        return account

    def update_account(self,
                       account: Account,
                       email: str = None,
                       phone: str = None,
                       first_name: str = None,
                       last_name: str = None,
                       secret_key: str = None,
                       avatar: dict = None,
                       status: str = None,
                       ):

        if phone:
            account.phone = phone

        if email:
            account.email = email

        if first_name:
            account.first_name = first_name

        if last_name:
            account.last_name = last_name

        if avatar:
            account.avatar = avatar

        if secret_key:
            account.secret_key = account.encrypt_secret_key(secret_key)

        if status:
            account.status = status

        self.session.add(account)

        return account
