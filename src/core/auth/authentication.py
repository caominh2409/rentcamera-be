import uuid
import secrets

from src.bases.core import Core
from src.bases.error.core import CoreError
# from src.models import (Authentication, AccessToken,
#                         Account)
from src.common.utils import get_now, make_jwt_token

from config import AUTH_EXPIRE_TIME, ACCESS_TOKEN_EXPIRE_TIME, SECRET_KEY


class AuthenticationLogic(Core):
    pass
    # def authenticate(
    #         self,
    #         email: str,
    #         secret_key: str,
    #         scope_type: str,
    #         scope_id: str = None,
    #         auth_expire_time: int = AUTH_EXPIRE_TIME,
    #         access_token_expire_time: int = ACCESS_TOKEN_EXPIRE_TIME
    # ) -> (Authentication, AccessToken):

    #     account = self.session.query(Account).filter(
    #         Account.email == email
    #     ).first()
    #     if not account:
    #         raise CoreError('InvalidCredentials')

    #     if not account.validate_secret_key(secret_key):
    #         raise CoreError('InvalidCredentials')

    #     auth = Authentication()
    #     auth.id = str(uuid.uuid4())
    #     auth.scope_type = scope_type
    #     auth.scope_id = scope_id
    #     auth.account = account
    #     auth.expired_at = get_now(add_time=auth_expire_time)

    #     self.session.add(auth)

    #     access_token = self.create_access_token(
    #         auth,
    #         expire_time=access_token_expire_time
    #     )

    #     return auth, access_token

    # def create_access_token(
    #         self,
    #         auth: Authentication,
    #         expire_time: int = ACCESS_TOKEN_EXPIRE_TIME
    # ) -> AccessToken:
    #     if auth.is_expired:
    #         raise CoreError('AuthExpired')

    #     access_token = AccessToken()
    #     access_token.auth = auth
    #     access_token.account = auth.account
    #     access_token.expired_at = get_now(add_time=expire_time)
    #     access_token.value = make_jwt_token(
    #         refresh_token=auth.id,
    #         secret_key=SECRET_KEY,
    #         expire_in=expire_time,
    #         account_id=auth.account.id,
    #         scope_type=auth.scope_type,
    #         scope_id=auth.scope_id
    #     )

    #     self.session.add(access_token)
    #     return access_token

    # def create_account(self,
    #                    type: str,
    #                    email: str,
    #                    phone: str = None,
    #                    first_name: str = None,
    #                    last_name: str = None,
    #                    secret_key: str = None,
    #                    avatar: dict = None,
    #                    ) -> Account:

    #     account = Account()
    #     account.email = email
    #     account.type = type
    #     account.phone = phone
    #     account.first_name = first_name
    #     account.last_name = last_name

    #     if avatar:
    #         account.avatar = avatar

    #     if not secret_key:
    #         secret_key = secrets.token_hex(24)

    #     account.secret_key = account.encrypt_secret_key(secret_key)

    #     self.session.add(account)

    #     return account
