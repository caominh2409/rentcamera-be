import datetime
import json

from src.bases.core import Core
from src.models import Logging
from src.common.json_encoders import CustomJsonEncoder


class LoggingLogic(Core):
    def create(self,
               type: str,
               tracing_id: str,
               meta: dict,
               created_at: datetime.datetime = None
               ):

        result = Logging()
        result.type = type
        result.tracing_id = tracing_id

        result.meta = json.loads(json.dumps(meta, cls=CustomJsonEncoder))

        if created_at:
            result.created_at = created_at

        self.session.add(result)

        return result
