from src.bases.core import Core
from src.models.smtp import Smtp


class SmtpLogic(Core):
    def create(self,
               scope_type: str,
               name: str,
               display_name: str,
               email: str,
               username: str,
               password: str,
               host: str,
               port: int,
               scope_id: str = None):
        result = Smtp()

        result.name = name
        result.display_name = display_name
        result.email = email
        result.username = username
        result.password = password
        result.host = host
        result.port = port
        result.scope_type = scope_type

        if scope_id:
            result.scope_id = scope_id

        self.session.add(result)

        return result

    def update(self,
               smtp: Smtp,
               name: str = None,
               display_name: str = None,
               email: str = None,
               username: str = None,
               password: str = None,
               host: str = None,
               port: int = None,
               ):

        if name:
            smtp.name = name
        if display_name:
            smtp.display_name = display_name
        if email:
            smtp.email = email
        if username:
            smtp.username = username
        if password:
            smtp.password = password
        if host:
            smtp.host = host
        if port:
            smtp.port = port

        self.session.add(smtp)

        return smtp

    def delete(self,
               smtp: Smtp,
               ):
        self.session.delete(smtp)

        return smtp
