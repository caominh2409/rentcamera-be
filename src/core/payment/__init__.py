import secrets
from copy import deepcopy

from src.bases.core import Core
from src.bases.error.core import CoreError
from src.clients.goquo_payment import GoquoPaymentClient
from src.services.iam import IamService
from src.common.utils import (make_jwt_token, decode_jwt_token, 
                              get_api_base_url, enc_base64, dec_base64)
from src.common.constants import PAYMENT_METHODS
# from src.models import (Payment, PaymentClientInfo,
#                         CreditCard, PaymentContact,
#                         App, SupplierSwitch, Supplier)
from src.core.currency import CurrencyLogic

from config import SECRET_KEY

class PaymentLogic(Core):
    pass

# class PaymentLogic(Core):
#     def init(self,
#              app: App,
#              flow_type: str,
#              method: str,
#              amount: float,
#              currency_info: dict,
#              client_info: dict,
#              desc: str,
#              shopping_id: str = None,
#              contact=None,
#              card_info=None,
#              meta=None,
#              ):
#         if not meta:
#             meta = {}

#         logic = CurrencyLogic(self.session)
#         try:
#             rounding = logic.currency_rounding(currency_info.get('origin'))
#             amount = round(amount, rounding)
#         except CoreError as e:
#             pass

#         payment = Payment()
#         payment.transaction_id = self._gen_transaction_id()
#         payment.client_info = PaymentClientInfo(**client_info)
#         payment.currency_info = currency_info
#         payment.amount = amount
#         payment.method = method
#         payment.flow_type = flow_type
#         payment.amount = amount
#         payment.desc = desc
#         payment.meta = meta
#         payment.app = app

#         if card_info:
#             card = CreditCard()
#             card.update(**card_info)
#             payment.credit_card = card

#         if shopping_id:
#             payment.shopping_id = shopping_id

#         if payment.flow_type == 'product-booking': 
#             if not contact:
#                 raise CoreError('MissingContact')
#             payment.contact = PaymentContact(**contact)

#             if not payment.shopping_id:
#                 raise CoreError('MissingShoppingId')

#             if payment.method == 'b2b-balance':
#                 if payment.currency_info['conversion'] != 'USD':
#                     raise CoreError('InvalidCurrency')

#             else:
#                 sw = self.session.query(SupplierSwitch).join(
#                     Supplier,
#                     Supplier.id == SupplierSwitch.supplier_id
#                 ).filter(
#                     SupplierSwitch.app_id == app.id,
#                     Supplier.type == 'payment',
#                 ).first()
#                 if not sw:
#                     raise CoreError(message='Switch not found')

#                 self._make_goquo_redirect_rq(
#                     payment=payment,
#                     gateway_settings=dict(
#                         urls=sw.supplier.urls,
#                         credentials=sw.credentials
#                     ),
#                 )

#         elif payment.flow_type == 'b2b-topup':
#             agent_user_id = payment.meta.get('agent_user_id')
#             if not agent_user_id:
#                 raise CoreError('MissingAgentUserId')
#             iam_service = IamService()
#             agent_user = iam_service.get_agent_user(
#                 id=agent_user_id
#             )
#             if not agent_user:
#                 raise CoreError('AgentUserNotFound')
#             payment.contact = PaymentContact(
#                 email=agent_user['email'],
#                 phone=agent_user.get('phone') or '+84935797283',
#                 last_name=agent_user.get('last_name') or 'Tri',
#                 first_name=agent_user.get('first_name') or 'Nguyen',
#                 country='GB'
#             )
#             self._make_goquo_redirect_rq(
#                 payment=payment,
#             )

#         else:
#             raise CoreError('UnsupportedFlow')

#         self.session.add(payment)

#         proceed_url = self._gen_proceed_url(
#             payment=payment,
#             app=app
#         )

#         return payment, proceed_url

#     def list_methods(self, currency_code, **kwargs):

#         result = []

#         for m in deepcopy(PAYMENT_METHODS):
#             if m['id'] == 'goquo':
#                 goquo_settings = kwargs.get('goquo_settings')
#                 if not goquo_settings:
#                     continue
#                 pm_client = GoquoPaymentClient(
#                     **goquo_settings
#                 )
#                 res = pm_client.list_options(currency_code)
#                 m['options'] = res['data']

#             result.append(m)

#         return result

#     def get_payment_from_token(self, token):
#         try:
#             data = decode_jwt_token(
#                 token=token,
#                 secret_key=SECRET_KEY,
#                 verify_exp=True
#             )
#             transaction_id = data['transaction_id']
#         except Exception as e:
#             raise CoreError('InvalidToken',
#                             message=str(e))

#         payment = self.session.query(Payment).filter(
#             Payment.transaction_id == transaction_id
#         ).first()
#         if not payment:
#             raise CoreError('InvalidToken')

#         return payment

#     def _gen_transaction_id(self):
#         result = secrets.token_hex(8)
#         if self.session.query(Payment).filter(
#                 Payment.transaction_id == result
#         ).first():
#             return self._gen_transaction_id()
#         return result

#     @staticmethod
#     def _make_goquo_redirect_rq(
#             gateway_settings,
#             payment,
#     ):
#         goquo_info = payment.meta.get('goquo')
#         if not goquo_info:
#             raise CoreError('MissingGoquoInfo')
#         option_id = goquo_info.get('option_id')
#         channel_id = goquo_info.get('channel_id')
#         if not option_id:
#             raise CoreError('MissingGoquoOptionId')
#         if not channel_id:
#             raise CoreError('MissingGoquoChannelId')

#         try:
#             goquo_pm_service = GoquoPaymentClient(
#                 **gateway_settings
#             )
#         except Exception as e:
#             raise CoreError(message=str(e))

#         try:
#             option, channel = goquo_pm_service.get_option(
#                 currency=payment.currency_info['conversion'],
#                 option_id=option_id,
#                 channel_id=channel_id
#             )
#         except Exception as e:
#             raise CoreError('CannotGetGoquoPaymentOption',
#                             message=str(e))

#         callback_url = f'{get_api_base_url()}/payments/callbacks/goquo'

#         gen_request_payload = dict(
#             payment_id=channel['payment_id'],
#             request_url=channel['request_url'],
#             callback_url=callback_url,
#             transaction_id=payment.transaction_id,
#             currency=payment.currency_info['conversion'],
#             amount=payment.amount,
#             desc=payment.desc,
#             method=option['payment_type'],
#             client_ip=payment.client_info.ip,
#             user_reference=payment.app.name[:22],
#             capture_card=option['capture_card'],
#             customer_info=[{
#                 'title': 'Mr',
#                 'email': payment.contact.email,
#                 'first_name': payment.contact.first_name,
#                 'last_name': payment.contact.last_name,
#                 'phone_number': payment.contact.phone,
#                 'country_code': payment.contact.country,
#                 'city': payment.contact.city,
#             }]
#         )

#         capture_card = option['capture_card']
#         if capture_card:
#             if not payment.credit_card:
#                 raise CoreError('MissingCardInfo')
            
#             gen_request_payload['card_info'] = {
#                 'cardholder_name': payment.credit_card.holder,
#                 'card_no': payment.credit_card.number,
#                 'cvv': payment.credit_card.cvv,
#                 'card_type': payment.credit_card.type,
#                 'exp_month': payment.credit_card.expire.month,
#                 'exp_year': payment.credit_card.expire.year
#             }

#         goquo_info['redirect_payload'] = goquo_pm_service.gen_request(
#             **gen_request_payload
#         )

#     @staticmethod
#     def _gen_proceed_url(payment, app=None):
#         token = make_jwt_token(
#             secret_key=SECRET_KEY,
#             expire_time=300,
#             transaction_id=payment.transaction_id
#         )
#         result = f'{get_api_base_url()}/payments/proceed?token={token}'
#         if app:
#             result += f'&app_id={app.id}'
#         return result
