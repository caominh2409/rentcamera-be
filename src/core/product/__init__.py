from src.bases.core import Core
from src.bases.error.core import CoreError
from src.models import Product
from sqlalchemy import or_

class ProductLogic(Core):
    def get_list(self,
                 search_text,
                 category_ids,
                 sorts,
                 page,
                 per_page):
        query = self.session.query(Product)
        st_params = []
        if search_text:
            st_params.append(Product.name.ilke(f'%{search_text}%'))
            st_params.append(Product.code.ilke(f'%{search_text}%'))
            st_params.append(Product.brand.ilke(f'%{search_text}%'))
        query.filter(or_(*st_params))

        total = query.count()
        query = query.sort(Product, *sorts).pagination(page, per_page)
        result = [i.to_json() for i in query]

        return total, result

    def create(self,
               name: str,
               code: str,
               brand: str,
               bio = "",
               description = "",
               ingredients = "",
               img = "",
               meta = {}
               ) -> Product:

        product = Product(
            name = name,
            code = code,
            brand = brand,
            bio = bio,
            description = description,
            ingredients = ingredients,
            img = img,
            meta = meta
        )

        self.session.add(product)

        return product

    def update(self,
               product: Product,
               name: str,
               code: str,
               brand: str,
               bio = "",
               description = "",
               ingredients = "",
               img = "",
               meta = {}
               ) -> Product:

        product.name = name
        product.code = code
        product.brand = brand
        product.bio = bio
        product.description = description
        product.ingredients = ingredients
        product.img = img
        product.meta = meta

        self.session.add(product)

        return product

    def delete(self, product: Product):
        self.session.delete(product)
        return product
