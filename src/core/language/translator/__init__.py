from src.bases.core import Core
from src.clients.google_cloud_api import GoogleCloudApi
from src.common.dict_utils import (
    dive_to_set_value, dive_to_get_value, flatten_keys
)


class Translator(Core):

    def __init__(self, *args, **kwargs):
        self._gcc = GoogleCloudApi()
        super(Translator, self).__init__(*args, **kwargs)

    def translate(self,
                  json_data,
                  from_language,
                  to_languages=None):
        if not to_languages:
            to_languages = list(map(
                lambda l: l.code,
                self.session.query(Language.code).filter(
                    Language.code != from_language
                ).distinct(Language.code)
            ))

        result = {}

        flattened_data = dict(
            ('.'.join(k), dive_to_get_value(data=json_data, path=k))
            for k in flatten_keys(data=json_data)
        )

        for lang_code in to_languages:
            lang_json = {}
            bulk_size = 50
            page = 1

            keys_and_words = list(flattened_data.items())
            while ((page - 1) * bulk_size) < len(keys_and_words):
                bulk = keys_and_words[(page - 1) * bulk_size: page * bulk_size]
                words = list(map(
                    lambda x: x[1],
                    bulk
                ))
                translated_words = self._gcc.translate_texts(
                    texts=words,
                    from_language=from_language,
                    to_language=lang_code
                )
                for index, translated_word in enumerate(translated_words):
                    key, word = bulk[index]
                    dive_to_set_value(
                        data=lang_json,
                        path=[key],
                        value=translated_word
                    )
                page += 1

            result[lang_code] = lang_json

        return result
