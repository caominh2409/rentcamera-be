from copy import deepcopy

from src.bases.core import Core
from src.bases.error.core import CoreError

from .translator import Translator

class LanguageLogic(Core):
    pass

# class LanguageLogic_App(Core):
#     def _get_default_language(self):
#         en = self.session.query(Language).filter(
#             Language.code == 'en-US',
#             Language.scope_type == 'System'
#         ).first()
#         return en

#     def create(self,
#                name: str,
#                code: str,
#                scope_type: str,
#                scope_id: str = None,
#                right_to_left: bool = None,
#                avatar: dict = None,
#                is_default: bool = None,
#                data: dict = None
#                ):

#         lang = Language(
#             name=name,
#             code=code,
#             scope_type=scope_type,
#             scope_id=scope_id
#         )
#         if avatar:
#             lang.avatar = avatar

#         if right_to_left is not None:
#             lang.right_to_left = right_to_left

#         self.validate_language(lang)

#         if is_default:
#             self.set_default_language(lang)

#         if data:
#             lang.data = data
#         else:
#             t = Translator(session=self.session)

#             default_language = self._get_default_language()

#             lang_jsons = t.translate(
#                 json_data=default_language.data,
#                 from_language=default_language.code,
#                 to_languages=[code]
#             )
#             lang.data = lang_jsons[code]
#         self.session.add(lang)

#         return lang

#     def update(self,
#                language: str,
#                name: str = None,
#                code: str = None,
#                right_to_left: bool = None,
#                data: dict = None,
#                avatar: dict = None,
#                enabled: bool = None,
#                is_default: bool = None
#                ):

#         if name:
#             language.name = name

#         if right_to_left is not None:
#             language.right_to_left = right_to_left

#         if code:
#             language.code = code

#         if data:
#             language.data = data

#         if avatar:
#             language.avatar = avatar

#         if enabled is not None:
#             language.enabled = enabled

#         self.validate_language(language)

#         if is_default:
#             self.set_default_language(language)

#         self.session.add(language)

#         return language

#     def validate_language(self, language: str):
#         query = self.session.query(Language).filter(
#             Language.code == language.code,
#             Language.scope_type == language.scope_type
#         )
#         if language.id:
#             query = query.filter(
#                 Language.id != language.id
#             )
#         if language.scope_id:
#             query = query.filter(
#                 Language.scope_id == language.scope_id
#             )
#         if query.first():
#             raise CoreError('InvalidParams', 'Duplicate langauge')

#     def merge_translations(self,
#                            language: Language,
#                            scope_type: str,
#                            scope_id: str = None,
#                            override: bool = False
#                            ):
#         origin_data = language.data
#         origin_keys = set(origin_data.keys())

#         t = Translator(session=self.session)

#         others = self.session.query(Language).filter(
#             Language.id != language.id,
#             Language.scope_type == scope_type
#         )
#         if scope_id:
#             others = others.filter(Language.scope_id == scope_id)

#         merged_languages = []

#         for other in others:
#             other_data = deepcopy(other.data)
#             other_keys = set(other_data.keys())
#             if override:
#                 diff_keys = origin_keys
#             else:
#                 diff_keys = origin_keys.difference(other_keys)

#             if not diff_keys:
#                 continue

#             need_to_translate = dict()
#             for dk in diff_keys:
#                 need_to_translate[dk] = origin_data[dk]

#             if other.code == language.code:
#                 translated_data = need_to_translate
#             else:
#                 translated_data = t.translate(
#                     json_data=need_to_translate,
#                     from_language=language.code,
#                     to_languages=[other.code]
#                 )[other.code]
#             for k, v in translated_data.items():
#                 other_data[k] = v
#             other.data = other_data

#             self.session.add(other)
#             merged_languages.append(other)

#         return merged_languages

#     def delete(self, language: Language):
#         if language.is_default:
#             raise CoreError('InvalidParams', 'Cannot delete default language')
#         self.session.delete(language)
#         return language

#     def set_default_language(self, language: Language):
#         others = self.session.query(Language).filter(
#             Language.scope_type == language.scope_type,
#             Language.is_default.is_(True),
#             Language.code != language.code
#         )
#         if language.scope_id:
#             others = others.filter(
#                 Language.scope_id == language.scope_id
#             )
#         if language.id:
#             others = others.filter(
#                 Language.id != language.id
#             )
#         for lang in others:
#             lang.is_default = False
#             self.session.add(lang)

#         language.is_default = True
#         self.session.add(language)
