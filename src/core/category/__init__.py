from src.bases.core import Core
from src.bases.error.core import CoreError
from src.models import Category
from sqlalchemy import or_

class CategoryLogic(Core):
    def get_list(self,
                 search_text,
                 sorts,
                 page,
                 per_page):
        query = self.session.query(Category)
        st_params = []
        if search_text:
            st_params.append(Category.name.ilke(f'%{search_text}%'))
            st_params.append(Category.code.ilke(f'%{search_text}%'))
            st_params.append(Category.brand.ilke(f'%{search_text}%'))
        query.filter(or_(*st_params))

        total = query.count()
        query = query.sort(Category, *sorts).pagination(page, per_page)
        result = [i.to_json() for i in query]

        return total, result

    def create(self,
               name: str,
               label: str,
               parent_id: str,
              ) -> Category:

        category = Category(
            name = name,
            label = label,
            parent_id = parent_id,
        )

        self.session.add(category)

        return category

    def update(self,
               category: Category,
               name: str,
               label: str,
               parent_id: str,
               ) -> Category:

        category.name = name
        category.label = label
        category.parent_id = parent_id

        self.session.add(category)

        return category

    def delete(self, category: Category):
        self.session.delete(category)
        return category
