import uuid

from src.bases.core import Core
from src.bases.error.core import CoreError
from src.models import Agent, AgentType, App
from src.core.balance import BalanceLogic


class B2bLogic(Core):
    def delete_agent(self, agent: Agent):
        self.session.delete(agent)
        return agent

    def delete_type(self, type: AgentType):
        self.session.delete(type)
        return type

    def create_agent(self,
                     app: App,
                     name: str,
                     email: str,
                     parent: Agent = None,
                     type: AgentType = None,
                     phone: str = None,
                     country: str = None,
                     address: str = None
                     ):

        result = Agent()
        result.app = app
        result.name = name
        result.email = email
        result.id = str(uuid.uuid4())

        if phone:
            result.phone = phone

        if country:
            result.country = country

        if address:
            result.address = address

        if parent:
            result.type_id = parent.type_id
            result.parent_id = parent.id
            parent_ids = []
            if parent.parent_ids:
                parent_ids = parent.parent_ids.split(',')
            parent_ids.append(parent.id)
            result.parent_ids = ','.join(parent_ids)
        else:
            if not type:
                raise CoreError('InvalidParams', 'Missing agent type')
            result.type_id = type.id

        self.validate_agent(result)
        self.session.add(result)

        if not parent:
            balance_logic = BalanceLogic(self.session)
            balance_logic.create(scope_id=result.id,
                                 scope_type=Agent.__name__)
        return result

    def update_agent(self,
                     agent,
                     type: AgentType = None,
                     email: str = None,
                     phone: str = None,
                     address: str = None,
                     country: str = None,
                     name: str = None,
                     enabled: bool = None,
                     ):

        if type and agent.type_id != type.id:
            agent.type_id = type.id
            agent.type = type

        if enabled is not None:
            agent.enabled = enabled

        if email and agent.email != email:
            agent.email = email

        agent.update(
            phone=phone,
            name=name,
            address=address,
            country=country
        )

        self.validate_agent(agent)

        self.session.add(agent)

        return agent

    def validate_agent(self, agent):
        query_params = [Agent.app_id == agent.app.id,
                        Agent.email == agent.email]
        if agent.created_at:
            query_params.append(Agent.id != agent.id)
        query = self.session.query(Agent).filter(*query_params)
        if query.first():
            raise CoreError('InvalidParams',
                            'Duplicate agent email')

    def create_type(self,
                    app: App,
                    name: str):
        agent_type = AgentType()
        agent_type.app_id = app.id
        agent_type.name = name

        self.validate_type(agent_type)

        self.session.add(agent_type)

        return agent_type

    def update_type(self,
                    type: AgentType,
                    name: str = None,
                    enabled: bool = None):
        if name:
            type.name = name
        if enabled is not None:
            type.enabled = enabled

        self.validate_type(type)

        self.session.add(type)

        return type

    def validate_type(self, type: AgentType):
        duplication_query = self.session.query(AgentType).filter(
            AgentType.name == type.name,
            AgentType.app_id == type.app_id
        )
        if type.created_at:
            duplication_query = duplication_query.filter(
                AgentType.id != type.id
            )
        if duplication_query.first():
            raise CoreError('InvalidParams', 'Duplicate name')
