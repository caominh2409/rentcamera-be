from src.bases.core import Core
from src.bases.error.core import CoreError
from src.models import Customer


class CustomerLogic(Core):
    def create(self,
               platform: str,
               uid: str,
               username: str,
               first_name: str,
               last_name: str,
               gender: str = None,
               phone: str = None,
               title: str = None,
               **kwargs
               ):
        customer = Customer()
        customer.platform = platform
        customer.uid = uid
        customer.username = username
        customer.first_name = first_name
        customer.last_name = last_name
        if gender:
            customer.gender = gender
        if phone:
            customer.phone = phone
        if title:
            customer.title = title

        self.validate(customer)
        self.session.add(customer)

        return customer

    def validate(self, customer: Customer):
        query = self.session.query(Customer).filter(
            Customer.platform == customer.platform,
            Customer.uid == customer.uid
        )

        if customer.created_at:
            query = query.filter(Customer.id != customer.id)

        if query.first():
            raise CoreError('InvalidParams', 'Duplicate customer.')
