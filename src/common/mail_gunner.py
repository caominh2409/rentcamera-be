import os.path
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


class MailGunner(object):
    def __init__(self, username, password, port, host):
        self.username = username
        self.password = password
        self.port = port
        self.host = host

    def send(self,
             from_name,
             to_addr,
             subject,
             body,
             cc=None,
             bcc=None,
             attachments=None):

        if not cc:
            cc = []

        if not bcc:
            bcc = []

        to_addrs = cc + bcc + [to_addr]
        msg = MIMEMultipart()
        msg.attach(MIMEText(body, 'html'))
        msg['Subject'] = subject
        msg['From'] = from_name
        msg['To'] = to_addr
        if cc:
            msg['CC'] = ','.join(cc)

        if attachments:
            for atm_path in attachments:
                atm_name = os.path.basename(atm_path)
                atm = MIMEBase('application', 'octet-stream')
                atm.set_payload(open(atm_path, 'rb').read())
                encoders.encode_base64(atm)
                atm.add_header(
                    'Content-Disposition',
                    f'attachment; filename={atm_name}'
                )
                msg.attach(atm)

        smtp = smtplib.SMTP(host=self.host,
                            port=self.port,
                            timeout=5)
        smtp.starttls()
        smtp.login(self.username, self.password)

        response = smtp.sendmail(
            from_addr=from_name,
            to_addrs=to_addrs,
            msg=msg.as_string()
        )

        return response
