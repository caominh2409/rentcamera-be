from src.bases.service import Service

from config import SERVICES


class IamService(Service):
    base_url = SERVICES['IAM']['url']

    def get_account(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/accounts',
            payload=payload
        )

    def create_account(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/accounts',
            payload=payload
        )

    def get_credentials(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/auth',
            payload=payload
        )
