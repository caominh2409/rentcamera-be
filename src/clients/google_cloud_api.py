import os
from google.oauth2.service_account import Credentials
from google.cloud import translate
from google.cloud.translate import TranslateTextRequest
from google.cloud import storage

from src.bases.error.client import ClientError

from config import ROOT_PATH, GCP_PROJECT, ENVIRONMENT


class GoogleCloudApi(object):
    def __init__(self):
        self.credentials = Credentials.from_service_account_file(
            os.path.join(ROOT_PATH, 'gcp.credentials.json')
        )
        self.translate_client = self._build_translate_client()
        self.storage_client = self._build_storage_client()
        self.project_id = GCP_PROJECT

    def _build_translate_client(self):
        return translate.TranslationServiceClient(
            credentials=self.credentials
        )

    def _build_storage_client(self):
        return storage.Client(
            credentials=self.credentials
        )

    def _get_storage_bucket(self):
        return self.storage_client.get_bucket(f'{ENVIRONMENT}-ota')

    def translate_texts(self,
                        texts,
                        from_language,
                        to_language):
        location = 'global'

        parent = f'projects/{self.project_id}/locations/{location}'

        # Detail on supported types can be found here:
        # https://cloud.google.com/translate/docs/supported-formats
        try:
            response = self.translate_client.translate_text(
                request=TranslateTextRequest({
                    'parent': parent,
                    'contents': texts,
                    'mime_type': 'text/plain',
                    'source_language_code': from_language,
                    'target_language_code': to_language,
                })
            )
        except Exception as e:
            raise ClientError(message=str(e))

        return [t.translated_text for t in response.translations]

    def upload_file(self, local_file_path, gs_file_path):
        bucket = self._get_storage_bucket()
        blob = bucket.blob(gs_file_path)
        blob.upload_from_filename(local_file_path)
        # blob.make_public()
        public_url = blob.public_url

        return public_url

    def download_file(self, path, save_path):
        bucket = self._get_storage_bucket()
        blob = bucket.get_blob(path)
        blob.download_to_filename(save_path)

    def remove_file(self, path, multi=False):
        bucket = self._get_storage_bucket()

        if multi:
            blobs = bucket.list_blobs(prefix=path)
            bucket.delete_blobs(blobs=list(blobs))

        else:
            blob = bucket.get_blob(path)
            if blob:
                blob.delete()
