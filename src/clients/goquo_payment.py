import secrets
import json
import math
import hashlib
import base64
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat import backends

from src.bases.request_handler import RequestHandler
from src.bases.error.client import ClientError
from src.common.utils import get_now

from config import ENVIRONMENT, BASE_DOMAIN


class DecryptionFailed(ClientError):
    pass


class InvalidData(ClientError):
    pass


class UnsupportedCardType(ClientError):
    pass


class GoquoPaymentClient(RequestHandler):
    def __init__(self, credentials, urls):
        self.credentials = credentials
        self.urls = urls

        self.rsa_backend = backends.default_backend()
        self.rsa_pub_key = serialization.load_pem_public_key(
            credentials['public_key'].encode(),
            self.rsa_backend)
        self.rsa_pri_key = serialization.load_pem_private_key(
            credentials['private_key'].encode(),
            password=None,
            backend=self.rsa_backend)

    def get_option(self, currency, option_id, channel_id):
        res = self.list_options(currency)
        print('here1', res)
        if not res.get('success'):
            return None

        options = res['data']
        print('here2', options)

        option = list(filter(lambda x: x['payment_optionid'] == option_id,
                             options))
        if not option:
            return None
        option = option[0]

        channel = list(filter(lambda x: x['payment_id'] == channel_id,
                              option['payment_channels']))
        print('here3', option)

        if not channel:
            return None
        channel = channel[0]

        return option, channel

    def list_options(self, currency):

        url = self.urls['get_options']

        payload = {
            'merchant_id': self.credentials['merchant_id'],
            'merchant_code': self.credentials['merchant_code'],
            'username': self.credentials['username'],
            'password': self.credentials['password'],
            'currency': currency,
            'country': self.credentials.get('country', 'ALL'),
            'product_code': self.credentials.get('product_code', 'ACT')
        }

        result = {
            'data': []
        }
        try:
            response = self._do_request(
                url=url,
                method='post',
                json=payload
            )
        except Exception as e:
            result['success'] = False
            result['message'] = '%s - %s' % (e.__class__, e)
            return result

        try:
            data = response.json()
        except json.JSONDecodeError:
            result['success'] = False
            result['message'] = 'JSONDecodeError'
            return result

        if response.status_code != 200:
            result['success'] = False
            return result

        result['data'] = data.get('payment_options', [])
        result['success'] = True
        return result

    def gen_request(self,
                    payment_id,
                    request_url,
                    callback_url,
                    transaction_id,
                    currency,
                    amount,
                    client_ip,
                    customer_info,
                    desc,
                    method='CreditCard',
                    user_reference=None,
                    capture_card=None,
                    card_info=None
                    ):

        payload = {
            'is_webrq': True,
            'language': 'GB',
            'merchantid': self.credentials['merchant_id'],
            'customerip': client_ip,
            'customer_info': customer_info,
            'merchant_rtn_url': callback_url,
            'merchant_callback_url': callback_url,
            'merchant_request_time': get_now().isoformat(),
            'signature': self._make_signature(currency=currency,
                                              amount=amount,
                                              transaction_id=transaction_id),
            'user_reference': user_reference
        }

        payment_info = {
            'merchant_txnid': transaction_id,
            'txn_amount': round(amount, 2),
            'TxnRqAmount': None,
            'RqMerchantTxnId': None,
            'txn_currency': currency,
            'TxnRqCurrency': None,
            'ActualAmount': 0.0,
            'ActualCurrency': None,
            'is_installment': False,
            'installment_period': 0,
            'installment_interesttype': None,
            # 'pnr': pnr,
            'PaymentMethod': method,
            'capture_card': capture_card,
        }

        if method == 'CreditCard':
            if capture_card:
                if not card_info:
                    raise ClientError(
                        'card_info is required for CreditCard method')

                payment_info['card_type'] = card_info.pop('card_type')
                payment_info['card_info'] = card_info

        payment_info['payment_id'] = payment_id
        payload['payment_info'] = payment_info
        payment_rq_url = request_url

        payload['order_info'] = {
            'order_desc': desc,
            'item_list': [{
                'item_id': 1,
                'item_code': desc,
                'item_type': 0,
                'item_desc': desc,
                'item_amount': amount,
                'RqItemAmount': None
            }]
        }
        random_password = self._make_random_password()

        encrypted_payment_rq = self._aes_encrypt(json.dumps(payload),
                                                 random_password)
        encrypted_password = self._rsa_encrypt(random_password)

        result = dict(
            enc_value=encrypted_password,
            transaction_id=transaction_id,
            merchant_id=self.credentials['merchant_id'],
            rq_data=encrypted_payment_rq,
            rq_url=payment_rq_url
        )

        return result

    def decrypt_response(self,
                         transaction_id: str,
                         encrypted_response: str,
                         enc_value: str) -> dict:
        try:
            password = self._rsa_decrypt(enc_value)
            payment_response = self._aes_decrypt(encrypted_response,
                                                 password)
            payment_response = json.loads(payment_response)
        except Exception as e:
            raise DecryptionFailed(message=str(e))

        res_transaction_id = payment_response['merchant_txnid']
        if res_transaction_id != transaction_id:
            raise InvalidData('MismatchedTransactionId')

        return payment_response

    def _make_signature(self, currency, amount, transaction_id):
        signature = currency + '{:.2f}'.format(amount) + transaction_id
        signature = hashlib.sha512(signature.upper().encode())
        return signature.hexdigest().upper()

    def _make_random_password(self):
        return secrets.token_hex(16)

    def _rsa_encrypt(self, data: str):
        data = data.ljust(256, ' ')
        encoded_data = data.encode()

        e = self.rsa_pub_key.public_numbers().e
        n = self.rsa_pub_key.public_numbers().n

        key_length = math.ceil(n.bit_length() / 8)

        encoded_data = int.from_bytes(encoded_data, byteorder='big')

        encrypted_data = pow(encoded_data, e, n)
        encrypted_data = encrypted_data.to_bytes(key_length, byteorder='big')

        return base64.b64encode(encrypted_data).decode()

    def _rsa_decrypt(self, data: str):
        encoded_data = data.encode()
        encoded_data = base64.b64decode(encoded_data)

        pub_key = self.rsa_pri_key.public_key()

        n = pub_key.public_numbers().n
        d = self.rsa_pri_key.private_numbers().d

        key_length = math.ceil(n.bit_length() / 8)
        decrypted_data = pow(int.from_bytes(encoded_data, byteorder='big'), d,
                             n)
        decrypted_data = decrypted_data.to_bytes(key_length, byteorder='big')
        decrypted_data = decrypted_data.decode()
        return decrypted_data[-32:]

    def _aes_encrypt(self, data: str, key: str) -> str:
        encoded_data = data.encode()

        cipher = self._make_aes_cipher(key)

        encryptor = cipher.encryptor()

        encrypted_data = encryptor.update(encoded_data) + encryptor.finalize()

        return base64.b64encode(encrypted_data).decode()

    def _aes_decrypt(self, encrypted_data: str, key: str):
        encrypted_data = base64.b64decode(encrypted_data)

        cipher = self._make_aes_cipher(key)
        decryptor = cipher.decryptor()

        result = decryptor.update(encrypted_data) + decryptor.finalize()

        return result.decode()

    def _make_aes_cipher(self, key: str):
        encoded_key = key.encode()

        backend = backends.default_backend()
        cipher = Cipher(algorithms.AES(encoded_key),
                        modes.CTR(self.credentials['iv_vector'].encode()),
                        backend=backend)
        return cipher
