from src.databases import Postgres, Mongo
from src.bases.api.factory import Factory
from src.api import resources
from src.workers.sender import sender as task_sender

from config import (POSTGRES_URI, ApiConfig, MONGO_URI)


def trigger_auto_sync(session):
    pass
    # app_sync_data = AppSyncLogic.detect_changes(session)

    # if app_sync_data:
    #     for app_id, payload in app_sync_data.items():
    #         task_sender.send_task(
    #             name='SyncingApp',
    #             queue='CommonTasks',
    #             kwargs=dict(
    #                 app_id=app_id,
    #                 sync_type='collections',
    #                 payload=payload
    #             )
    #         )


sql_db = Postgres(POSTGRES_URI)
sql_session_factory = sql_db.create_session_factory(
    disable_autoflush=True,
    callback_events={
        'after_commit': trigger_auto_sync
    }
)
mongo = Mongo(MONGO_URI)

factory = Factory(
    config=ApiConfig,
    sql_session_factory=sql_session_factory,
    resource_module=resources,
    mongo=mongo
)

app = factory.create_app()
