from src.bases.api.resource import Resource


class ProductResource(Resource):
    endpoint = '/products'
