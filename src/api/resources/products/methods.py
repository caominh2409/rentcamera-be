from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.models import Product
from src.core.product import ProductLogic
from .schemas import GetSchema, PostSchema, PatchSchema, DeleteSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        logic = ProductLogic(self.session)
        try:
            total, result = logic.get_list(
                search_text = self.payload.get("search_text"),
                sorts = self.payload.get('sorts', []),
                page = self.payload.get('page'),
                per_page = self.payload.get('per_page'),
                category_ids = self.payload.get('category_ids'),
            )
        except Exception as e:
            raise ServerError(e.error, e.message)
            
        return dict(
            status=True,
            message='',
            data={
                'total': total,
                'result': result
            }
        )

class Post(MethodHandler):
    input_schema_class = PostSchema

    def handle_logic(self):        
        logic = ProductLogic(self.session)
        try:
            result = logic.create(
                name=self.payload.get('name'),
                code=self.payload.get('code'),
                brand=self.payload.get('brand'),
                bio=self.payload.get('bio'),
                description=self.payload.get('description='),
                ingredients=self.payload.get('ingredients'),
                img=self.payload.get('img'),
                meta=self.payload.get('meta'),
            )
        except Exception as e:
            raise ServerError(e.error, e.message)

        self.session.commit()
        
        return dict(
            status=True,
            message='',
            data={
                'result': result.to_json()
            }
        )


class Patch(MethodHandler):
    input_schema_class = PatchSchema

    def handle_logic(self):        
        product = self.session.query(Product).get(
            self.payload['id']
        )
        if not product:
            raise ServerError('ProductBlockingNotFound')

        logic = ProductLogic(self.session)
        result = logic.update(
            product=product,
            name=self.payload.get('name'),
            code=self.payload.get('code'),
            brand=self.payload.get('brand'),           
            bio=self.payload.get('bio'),
            description=self.payload.get('description'),
            ingredients=self.payload.get('ingredients'),
            img=self.payload.get('img'),
            meta=self.payload.get('meta'),            
        )

        self.session.commit()

        return dict(
            status=True,
            message='',
            data={
                'result': result.to_json()
            }
        )

class Delete(MethodHandler):
    input_schema_class = DeleteSchema

    def handle_logic(self):
        id = self.payload.get('id')
        logic = ProductLogic(self.session) 
        product = self.session.query(Product).get(id)

        if not product:
            raise ServerError('ProductBlockingNotFound')
        
        logic.delete(product)
        self.session.commit()

        return dict(success=True)

