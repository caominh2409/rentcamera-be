from src.bases.schema import (IdField, BaseSchema, StringField,
                              ListField, BaseListingSchema,
                              STRING_LENGTH_VALIDATORS, fields)


class GetSchema(BaseListingSchema):
    search_text = ListField(StringField, allow_none=True)
    category_ids = StringField(required=False,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

class PostSchema(BaseSchema):
    name = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    code = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    img = StringField(allow_none=True,
                             validate=STRING_LENGTH_VALIDATORS['LONG'])
    bio = StringField(allow_none=True,
                       validate=STRING_LENGTH_VALIDATORS['LONG'])
    description = StringField(allow_none=True,
                        validate=STRING_LENGTH_VALIDATORS['LARGE'])
    ingredients = StringField(allow_none=True,
                        validate=STRING_LENGTH_VALIDATORS['LONG'])
    brand = StringField(required=True,
                        validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    meta = fields.Dict(allow_none=False)


class PatchSchema(BaseSchema):
    name = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    code = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    img = StringField(allow_none=True,
                             validate=STRING_LENGTH_VALIDATORS['LONG'])
    bio = StringField(allow_none=True,
                       validate=STRING_LENGTH_VALIDATORS['LONG'])
    description = StringField(allow_none=True,
                        validate=STRING_LENGTH_VALIDATORS['LARGE'])
    ingredients = StringField(allow_none=True,
                        validate=STRING_LENGTH_VALIDATORS['LONG'])
    brand = StringField(required=True,
                        validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    meta = fields.Dict(allow_none=False)


class DeleteSchema(BaseSchema):
    id = IdField(required=True)