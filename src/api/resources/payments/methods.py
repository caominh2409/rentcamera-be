from src.bases.api.method_handler import MethodHandler
# from src.models import Payment
from src.bases.error.api import BadRequestParams

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        transaction_id = self.payload.get('transaction_id')
        payment_id = self.payload.get('id')

        if not transaction_id and not payment_id:
            raise BadRequestParams(
                message='transaction_id and payment_id cannot be both empty'
            )
        if payment_id:
            payment = self.session.query(Payment).get(payment_id)
        else:
            payment = self.session.query(Payment).filter(
                Payment.transaction_id == transaction_id
            ).first()
        if not payment:
            raise BadRequestParams(message='Payment not found')

        return payment.to_json()
