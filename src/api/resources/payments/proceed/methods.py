import os
import json
from flask import redirect, make_response

from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams
from src.core.payment import PaymentLogic
from src.core.balance import BalanceLogic
from src.core.b2b import B2bLogic
from src.common.utils import gen_html
from src.common.json_encoders import CustomJsonEncoder

from config import ROOT_PATH

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        pms = PaymentLogic(self.session)

        payment = pms.get_payment_from_token(
            token=self.payload['token']
        )
        if payment.status != 'processing':
            raise BadRequestParams('PaymentAlreadyProceed')

        if payment.method == 'goquo':
            form_path = os.path.join(
                ROOT_PATH,
                'static/payment/goquo_payment_form.html'
            )
            with open(form_path, 'r') as file_ref:
                html_form = gen_html(
                    data=payment.meta['goquo']['redirect_payload'],
                    content=file_ref.read()
                )

            response = dict(
                type='html',
                data=html_form
            )

        elif payment.method == 'b2b-balance':
            agent_user = payment.agent_user
            b2b_service = B2bLogic(self.session)
            bs = BalanceLogic(self.session)
            balance = b2b_service.get_balance(agent_user.agent)
            if not balance:
                raise BadRequestParams('BalanceNotFound')
            bs.charge(
                balance=balance,
                payment=payment,
                description=f'{agent_user.email} booked products'
            )
            payment.status = 'success'
            self.session.add(payment)
            redirect_url = payment.client_info.redirect_url
            transaction_id = payment.transaction_id
            response = dict(
                type='redirect',
                data=redirect_url + f'?transaction_id={transaction_id}'
            )

        else:
            raise BadRequestParams('UnsupportedMethod')

        self.session.commit()

        return response

    def run(self):
        """The main flow of this HTTP method"""

        result = self.handle_logic()

        if result['type'] == 'redirect':
            return redirect(result['data'])

        if result['type'] == 'html':
            response = make_response(result['data'], 200)
            response.headers['Content-Type'] = 'text/html'
            return response

        return make_response(json.dumps(result,
                                        cls=CustomJsonEncoder), 200)

