from src.bases.schema import (BaseSchema, IdField, StringField,
                              fields, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class GetSchema(BaseSchema):
    token = StringField(required=True)
