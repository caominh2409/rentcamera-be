from src.bases.api.resource import Resource


class PaymentProceedResource(Resource):
    endpoint = '/payments/proceed'
