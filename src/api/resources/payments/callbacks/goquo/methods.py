import copy
from flask import redirect

from src.bases.api.method_handler import MethodHandler
# from src.models import Payment, SupplierSwitch, Supplier
from src.bases.error.api import BadRequestParams
from src.clients.goquo_payment import GoquoPaymentClient
from src.common.utils import get_now
from src.common.constants import GOQUO_PAYMENT_CREDENTIALS


from .schemas import PostSchema


class Post(MethodHandler):
    input_schema_class = PostSchema

    def _fetch_payload(self):
        if self.request.form:
            return self.request.form.copy()
        return {}

    def handle_logic(self):
        transaction_id = self.payload['MerchantTxnId']

        payment = self.session.query(Payment).filter(
            Payment.transaction_id == transaction_id
        ).first()
        if not payment or payment.status != 'processing':
            raise BadRequestParams('InvalidData')

        if payment.flow_type == 'product-booking':
            sw = self.session.query(SupplierSwitch).join(
                Supplier,
                Supplier.id == SupplierSwitch.supplier_id
            ).filter(
                Supplier.type == 'payment',
                SupplierSwitch.enabled.is_(True),
                SupplierSwitch.app_id == payment.app_id,
            ).first()
            if not sw:
                raise BadRequestParams('GoquoPaymentGatewayNotAvailable')

            pm_client = GoquoPaymentClient(
                credentials=sw.credentials,
                urls=sw.supplier.urls
            )

            payment_result = self._decrypt_goquo_response(
                pm_client=pm_client
            )

        elif payment.flow_type == 'b2b-topup':
            pm_client = GoquoPaymentClient(
                **GOQUO_PAYMENT_CREDENTIALS
            )
            payment_result = self._decrypt_goquo_response(
                pm_client=pm_client
            )

        else:
            raise BadRequestParams('UnsupportedFlowType')

        meta = copy.deepcopy(payment.meta)
        paymentrs_info = payment_result['paymentrs_info']
        txn_statusdesc = paymentrs_info['txn_statusdesc']

        meta['goquo']['response'] = paymentrs_info

        if txn_statusdesc in ['APPROVED', 'PREAUTHORIZED']:
            payment_status = 'success'
            payment.updated_at = get_now()

        elif txn_statusdesc == 'DECLINED':
            payment_status = 'failed'

        elif txn_statusdesc == 'WAITTOPAY':
            payment_status = 'processing'
        else:
            payment_status = 'failed'

        payment.status = payment_status
        payment.meta = meta

        if payment_status != 'success':
            payment.desc = paymentrs_info.get('provider_desc')

        self.session.add(payment)

        if payment.flow_type == 'b2b-topup' and payment.status == 'success':
            balance_service = BalanceService(self.session)
            b2b_service = B2bService(self.session)
            balance = b2b_service.get_balance(payment.agent_user.agent)
            balance_service.topup(balance=balance, payment=payment)

        self.session.commit()

        redirect_url = payment.client_info.redirect_url
        return redirect_url + f'?transaction_id={transaction_id}'

    def _decrypt_goquo_response(self, pm_client):
        transaction_id = self.payload['MerchantTxnId']
        try:
            payment_result = pm_client.decrypt_response(
                transaction_id=transaction_id,
                enc_value=self.payload['EncValue'],
                encrypted_response=self.payload['PaymentRS'],
            )
        except Exception as e:
            raise BadRequestParams('InvalidData',
                                   message=str(e))

        return payment_result

    def run(self):
        result = self.handle_logic()

        return redirect(result)
