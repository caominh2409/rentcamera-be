from src.bases.schema import (BaseSchema, IdField, StringField,
                              fields, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class PostSchema(BaseSchema):
    MerchantTxnId = StringField(required=True,
                                validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    PaymentRS = StringField(required=True)
    EncValue = StringField(required=True)
