from src.bases.api.resource import Resource


class PaymentGoquoCallbackResource(Resource):
    endpoint = '/payments/callbacks/goquo'
