from src.bases.schema import (IdField, BaseSchema, StringField,
                              STRING_LENGTH_VALIDATORS)


class GetSchema(BaseSchema):
    id = IdField(allow_none=True)

    transaction_id = StringField(allow_none=True,
                                 validate=STRING_LENGTH_VALIDATORS['LONG'])
