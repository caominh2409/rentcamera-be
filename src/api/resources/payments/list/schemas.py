from src.bases.schema import (IdField, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class GetSchema(BaseListingSchema):
    scope_id = ListField(IdField, allow_none=True)
    scope_type = ListField(StringField, allow_none=True)
    search_text = ListField(StringField, allow_none=True)
    code = ListField(StringField, allow_none=True)
    created_before = ListField(DatetimeField, allow_none=True)
    created_after = ListField(DatetimeField, allow_none=True)
