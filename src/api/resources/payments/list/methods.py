from sqlalchemy import or_

from src.bases.api.method_handler import MethodHandler
from src.models import Product

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        scope_id = self.payload.get('scope_id')
        scope_type = self.payload.get('scope_type')
        search_text = self.payload.get('search_text')
        code = self.payload.get('code')
        created_before = self.payload.get('created_before')
        created_after = self.payload.get('created_after')

        query = self.session.query(Product)

        if search_text:
            st_params = []
            for st in search_text:
                st_params.append(Product.name.ilike(f'%{st}%'))
            query = query.filter(or_(*st_params))

        if code:
            query = query.filter(Product.code.in_(code))

        if scope_id:
            query = query.filter(Product.scope_id.in_(scope_id))

        if scope_type:
            query = query.filter(Product.scope_type.in_(scope_type))

        if created_before:
            cb_params = []
            for cb in created_before:
                cb_params.append(Product.created_at <= cb)
            query = query.filter(or_(*cb_params))

        if created_after:
            ca_params = []
            for ca in created_after:
                ca_params.append(Product.created_at >= ca)
            query = query.filter(or_(*ca_params))

        total = query.count()

        result = []
        for i in query.paginate(
                page=self.payload.get('page'),
                per_page=self.payload.get('per_page')
        ):
            record = i.to_json()
            result.append(record)

        return dict(
            total=total,
            result=result
        )
