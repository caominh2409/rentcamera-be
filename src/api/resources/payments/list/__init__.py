from src.bases.api.resource import Resource


class PaymentListResource(Resource):
    endpoint = '/payments/list'
