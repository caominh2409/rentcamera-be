from src.bases.api.method_handler import MethodHandler
# from src.models import DockerImage
from src.bases.error.api import BadRequestParams

from .schemas import PostSchema


class Post(MethodHandler):
    input_schema_class = PostSchema

    def _fetch_payload(self):
        if self.request.form:
            return self.request.form.copy()
        return {}

    def handle_logic(self):
        type = self.payload['type']
        version = self.payload['version']
        name = self.payload['name']

        if self.session.query(DockerImage).filter(
            DockerImage.name == name,
            DockerImage.version == version,
            DockerImage.type == type,
        ).first():
            raise BadRequestParams(message='Image already exists')

        di = DockerImage()
        di.name = name
        di.type = type
        di.version = version

        self.session.add(di)
        self.session.commit()

        return di.to_json()
