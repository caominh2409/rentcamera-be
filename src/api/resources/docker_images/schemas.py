from src.bases.schema import (BaseSchema, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class PostSchema(BaseSchema):
    name = StringField(required=True,
                       validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    type = StringField(required=True,
                       validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    version = StringField(required=True,
                          validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
