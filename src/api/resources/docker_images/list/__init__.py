from src.bases.api.resource import Resource


class DockerImageListResource(Resource):
    endpoint = '/docker-images/list'
