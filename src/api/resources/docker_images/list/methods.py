from sqlalchemy import or_

from src.bases.api.method_handler import MethodHandler
# from src.models import DockerImage

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        search_text = self.payload.get('search_text')
        created_before = self.payload.get('created_before')
        created_after = self.payload.get('created_after')
        type = self.payload.get('type')

        query = self.session.query(DockerImage)

        if search_text:
            st_params = []
            for st in search_text:
                st_params.append(DockerImage.version.ilike(f'%{st}%'))
            query = query.filter(or_(*st_params))

        if type:
            query = query.filter(DockerImage.type.in_(type))

        if created_before:
            cb_params = []
            for cb in created_before:
                cb_params.append(DockerImage.created_at <= cb)
            query = query.filter(or_(*cb_params))

        if created_after:
            ca_params = []
            for ca in created_after:
                ca_params.append(DockerImage.created_at >= ca)
            query = query.filter(or_(*ca_params))

        total = query.count()

        result = []
        for i in query.sort(
                DockerImage,
                *(self.payload.get('sorts') or [])
        ).paginate(
            page=self.payload.get('page'),
            per_page=self.payload.get('per_page')
        ):
            record = i.to_json()
            result.append(record)

        return dict(
            total=total,
            result=result
        )
