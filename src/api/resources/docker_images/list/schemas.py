from src.bases.schema import (BaseSchema, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class GetSchema(BaseListingSchema):
    search_text = ListField(StringField, allow_none=True)
    type = ListField(StringField, allow_none=True)
    created_before = ListField(DatetimeField, allow_none=True)
    created_after = ListField(DatetimeField, allow_none=True)
