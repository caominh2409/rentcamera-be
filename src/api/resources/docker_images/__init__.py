from src.bases.api.resource import Resource


class DockerImageResource(Resource):
    endpoint = '/docker-images'
