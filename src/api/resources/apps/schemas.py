from src.bases.schema import (BaseSchema, IdField, fields,
                              StringField, STRING_LENGTH_VALIDATORS)


class GetSchema(BaseSchema):
    id = IdField(required=True)


class PatchSchema(BaseSchema):
    id = IdField(required=True)
    version_id = IdField(allow_none=True)

    name = StringField(allow_none=True,
                       validate=STRING_LENGTH_VALIDATORS['LONG'])

    general_settings = fields.Dict(allow_none=True)
    cms_settings = fields.Dict(allow_none=True)
    datetime_settings = fields.Dict(allow_none=True)
    booking_settings = fields.Dict(allow_none=True)
    notification_settings = fields.Dict(allow_none=True)
    currency_settings = fields.Dict(allow_none=True)
    age_policy = fields.Dict(allow_none=True)
