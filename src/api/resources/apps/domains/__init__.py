from src.bases.api.resource import Resource


class AppDomainResource(Resource):
    endpoint = '/apps/domains'
