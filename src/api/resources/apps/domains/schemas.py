from src.bases.schema import (IdField, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              BaseSchema)


class GetSchema(BaseListingSchema):
    app_id = IdField(required=True)

    search_text = ListField(StringField, allow_none=True)
    created_before = ListField(DatetimeField, allow_none=True)
    created_after = ListField(DatetimeField, allow_none=True)


class PostSchema(BaseSchema):
    app_id = IdField(required=True)

    value = StringField(required=True)


class DeleteSchema(BaseSchema):
    ids = ListField(IdField, required=True)

