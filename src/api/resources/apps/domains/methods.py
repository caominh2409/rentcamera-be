from sqlalchemy import or_

from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.bases.error.core import CoreError
# from src.models import AppCustomDomain, App
from src.core.app.domain import AppDomainLogic

from .schemas import GetSchema, PostSchema, DeleteSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        search_text = self.payload.get('search_text')
        created_before = self.payload.get('created_before')
        created_after = self.payload.get('created_after')

        query = self.session.query(
            AppCustomDomain
        ).filter(AppCustomDomain.app_id == self.payload['app_id'])

        if search_text:
            st_params = []
            for st in search_text:
                st_params.append(AppCustomDomain.value.ilike(f'%{st}%'))
            query = query.filter(or_(*st_params))

        if created_before:
            cb_params = []
            for cb in created_before:
                cb_params.append(AppCustomDomain.created_at <= cb)
            query = query.filter(or_(*cb_params))

        if created_after:
            ca_params = []
            for ca in created_after:
                ca_params.append(AppCustomDomain.created_at >= ca)
            query = query.filter(or_(*ca_params))

        total = query.count()

        result = []
        for i in query.paginate(
                page=self.payload.get('page'),
                per_page=self.payload.get('per_page')
        ):
            record = i.to_json()
            result.append(record)

        return dict(
            total=total,
            result=result
        )


class Post(MethodHandler):
    input_schema_class = PostSchema

    def handle_logic(self):
        app = self.session.query(App).get(self.payload['app_id'])
        if not app:
            raise BadRequestParams('AppNotFound')

        logic = AppDomainLogic(self.session)

        try:
            result = logic.create(
                app=app,
                value=self.payload['value']
            )
        except CoreError as e:
            if e.error in [
                'DomainAlreadyExits',
                'InvalidHost'
            ]:
                raise BadRequestParams(e.error, e.message)
            raise ServerError(e.error, e.message)

        self.session.commit()

        return result.to_json()


class Delete(MethodHandler):
    input_schema_class = DeleteSchema

    def handle_logic(self):

        logic = AppDomainLogic(self.session)

        for id in self.payload['ids']:
            domain = self.session.query(AppCustomDomain).get(id)
            if not domain:
                continue
            logic.delete(domain)
        self.session.commit()

        return dict(success=True)
