from src.bases.api.resource import Resource


class AppSettingResource(Resource):
    endpoint = '/apps/settings'
