from src.bases.schema import (BaseSchema, IdField, fields, ListField,
                              StringField, STRING_LENGTH_VALIDATORS)


class GetSchema(BaseSchema):
    app_id = IdField(required=True)

    keys = ListField(
        StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT']),
        required=True
    )
