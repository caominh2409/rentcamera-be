from src.bases.api.method_handler import MethodHandler
# from src.models import App, AppVersion
from src.bases.error.api import BadRequestParams, ServerError
from src.bases.error.core import CoreError
from src.core.app import AppLogic
from src.workers.sender import sender as task_sender

from .schemas import GetSchema, PatchSchema


def parse_app(app: str) -> dict:
    result = app.to_json(
        exclude_fields=['secret_key']
    )

    result['default_domain'] = app.default_domain

    for key in [
        'general_settings',
        'currency_settings',
        'datetime_settings',
        'booking_settings',
        'notification_settings',
        'cms_settings',
        'age_policy',
    ]:
        settings = getattr(app, key, None)
        if settings:
            result[key] = settings.to_json()

    return result


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        app = self.session.query(App).get(self.payload['id'])
        if not app:
            raise BadRequestParams('AppNotFound')

        return parse_app(app)


class Patch(MethodHandler):
    input_schema_class = PatchSchema

    def handle_logic(self):
        app = self.session.query(App).get(self.payload['id'])
        if not app:
            raise BadRequestParams('AppNotFound')

        params = dict(
            app=app,
            name=self.payload.get('name'),
            general_settings=self.payload.get('general_settings'),
            cms_settings=self.payload.get('cms_settings'),
            datetime_settings=self.payload.get('datetime_settings'),
            booking_settings=self.payload.get('booking_settings'),
            currency_settings=self.payload.get('currency_settings'),
            notification_settings=self.payload.get('notification_settings'),
            age_policy=self.payload.get('age_policy'),
        )

        version_id = self.payload.get('version_id')
        if version_id:
            version = self.session.query(AppVersion).get(version_id)
            if not version:
                raise BadRequestParams(message='Version not found')

            params['version'] = version

        logic = AppLogic(self.session)

        try:
            app, processes = logic.update(
                account_id=self.credentials['account']['id'],
                **params
            )
        except CoreError as e:
            if e.error == 'InvalidParams':
                raise BadRequestParams(message=e.message)
            raise ServerError(e.message)

        self.session.commit()

        json_processes = []

        for p in processes:
            task_sender.send_task(
                name='HandlingProcesses',
                queue='CommonTasks',
                kwargs=dict(process_id=p.id)
            )
            json_processes.append(p.to_json())

        return dict(
            app=parse_app(app),
            processes=json_processes
        )
