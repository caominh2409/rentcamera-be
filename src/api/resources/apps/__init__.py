from src.bases.api.resource import Resource


class AppResource(Resource):
    endpoint = '/apps'
