from src.bases.api.resource import Resource


class CategoryResource(Resource):
    endpoint = '/category'
