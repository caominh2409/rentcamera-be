from src.bases.schema import (IdField, BaseSchema, StringField,
                              ListField, BaseListingSchema,
                              STRING_LENGTH_VALIDATORS, fields)


class GetSchema(BaseListingSchema):
    search_text = ListField(StringField, allow_none=True)

class PostSchema(BaseSchema):
    name = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    label = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    parent_id = StringField(allow_none=True,
                             validate=STRING_LENGTH_VALIDATORS['LONG'])


class PatchSchema(BaseSchema):
    name = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    label = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    parent_id = StringField(allow_none=True,
                             validate=STRING_LENGTH_VALIDATORS['LONG'])


class DeleteSchema(BaseSchema):
    id = IdField(required=True)