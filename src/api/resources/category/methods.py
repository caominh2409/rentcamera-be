from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.models import Category
from src.core.category import CategoryLogic
from .schemas import GetSchema, PostSchema, PatchSchema, DeleteSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        logic = CategoryLogic(self.session)
        try:
            total, result = logic.get_list(
                search_text = self.payload.get("search_text"),
                sorts = self.payload.get('sorts', []),
                page = self.payload.get('page'),
                per_page = self.payload.get('per_page'),
            )
        except Exception as e:
            raise ServerError(e.error, e.message)
            
        return dict(
            status=True,
            message='',
            data={
                'total': total,
                'result': result
            }
        )

class Post(MethodHandler):
    input_schema_class = PostSchema

    def handle_logic(self):        
        logic = CategoryLogic(self.session)
        try:
            result = logic.create(
                name=self.payload.get('name'),
                label=self.payload.get('label'),
                parent_id=self.payload.get('parent_id'),
            )
        except Exception as e:
            raise ServerError(e.error, e.message)

        self.session.commit()
        
        return dict(
            status=True,
            message='',
            data={
                'result': result.to_json()
            }
        )


class Patch(MethodHandler):
    input_schema_class = PatchSchema

    def handle_logic(self):        
        category = self.session.query(Category).get(
            self.payload['id']
        )
        if not category:
            raise ServerError('CategoryNotFound')

        logic = CategoryLogic(self.session)
        result = logic.update(
            category=category,
            name=self.payload.get('name'),
            label=self.payload.get('label'),
            parent_id=self.payload.get('parent_id'),                      
        )

        self.session.commit()

        return dict(
            status=True,
            message='',
            data={
                'result': result.to_json()
            }
        )

class Delete(MethodHandler):
    input_schema_class = DeleteSchema

    def handle_logic(self):
        id = self.payload.get('id')
        logic = CategoryLogic(self.session) 
        category = self.session.query(Category).get(id)

        if not category:
            raise ServerError('CategoryNotFound')
        
        logic.delete(category)
        self.session.commit()

        return dict(success=True)

