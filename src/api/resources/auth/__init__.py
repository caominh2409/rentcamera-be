from src.bases.api.resource import Resource


class AuthResource(Resource):
    endpoint = '/auth'
