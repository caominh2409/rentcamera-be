from src.bases.schema import (BaseSchema, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              STRING_LENGTH_VALIDATORS)


class PostSchema(BaseSchema):
    email = fields.Email(required=True,
                         validate=STRING_LENGTH_VALIDATORS['LONG'])
    scope_type = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    scope_id = StringField(allow_none=True,
                           validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    secret_key = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['LONG'])


class GetSchema(BaseSchema):
    access_token = StringField(required=True,
                               alidate=STRING_LENGTH_VALIDATORS['LARGE'])
