from src.bases.schema import (BaseSchema, BaseListingSchema, fields,
                              StringField, ListField, DatetimeField,
                              STRING_LENGTH_VALIDATORS, IdField)


class PostSchema(BaseSchema):
    platform_id = StringField(required=True,
                              validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    callback_url = StringField(required=True,
                               validate=STRING_LENGTH_VALIDATORS['LARGE'])

    request_id = IdField(required=True)
    account_ikd = IdField(allow_none=True)
