from uuid import uuid4
from google_auth_oauthlib import flow
from flask import url_for
from urllib.parse import urlencode

from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams
from src.models.account import Account
from src.common.utils import get_api_base_url
from src.business_logics import BusinessLogic

from config import GCP, FACEBOOK

from .schemas import PostSchema


class Post(MethodHandler):
    input_schema_class = PostSchema

    def handle_logic(self):
        platform_id = self.payload['platform_id']
        callback_url = self.payload['callback_url']
        request_id = self.payload['request_id']

        cache_data = dict(
            callback_url=callback_url,
            request_id=request_id
        )

        if platform_id == 'google':
            auth_flow = flow.Flow.from_client_config(
                GCP['oauth'],
                scopes=[
                    'openid',
                    'https://www.googleapis.com/auth/userinfo.profile',
                    'https://www.googleapis.com/auth/userinfo.email'
                ],
                redirect_uri=self._make_redirect_url(platform_id)
            )
            authorization_url, state = auth_flow.authorization_url(
                access_type='offline',
                include_granted_scopes='true'
            )
            cache_key = f'google-oauth:{state}'

        elif platform_id == 'facebook':
            state = str(uuid4())
            cache_key = f'facebook-oauth:{state}'

            query = {
                'client_id': FACEBOOK['app_id'],
                'state': state,
                'redirect_uri': self._make_redirect_url(platform_id),
                'scope': 'public_profile,email,user_birthday'
            }
            authorization_url = 'https://www.facebook.com/v11.0/dialog/oauth'
            authorization_url += f'?{urlencode(query)}'

        else:
            raise BadRequestParams('UnsupportedPlatform')

        self.redis.set(cache_key, cache_data)

        return dict(
            redirect_url=authorization_url
        )

    @staticmethod
    def _make_redirect_url(platform_id):
        resource_map = {
            'google': url_for('GoogleAuthCallbackResource'),
            'facebook': url_for('FacebookAuthCallbackResource'),
        }

        return get_api_base_url() + resource_map[platform_id]

    def _get_current_url(self):
        return get_api_base_url() + self.request.full_path

