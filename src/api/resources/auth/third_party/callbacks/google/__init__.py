from src.bases.api.resource import Resource


class GoogleAuthCallbackResource(Resource):
    endpoint = '/auth/3rd-party/callbacks/google'
