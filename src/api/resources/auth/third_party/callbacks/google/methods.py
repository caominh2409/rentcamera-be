import requests
from google_auth_oauthlib import flow
from oauthlib.oauth2.rfc6749 import errors
from flask import url_for, redirect
from urllib import parse

from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.common.utils import get_api_base_url
from src.business_logics import BusinessLogic
from src.models.account import ThirdPartyPlatform

from config import GCP

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        platform_id = 'google'

        state = self.payload['state']

        cache_key = f'google-oauth:{state}'

        cache_data = self.redis.get(cache_key)
        if not cache_data:
            raise ServerError('CacheDataNotFound')

        auth_flow = flow.Flow.from_client_config(
            GCP['oauth'],
            scopes=[
                'openid',
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email'
            ],
            state=state,
            redirect_uri=get_api_base_url() + url_for(
                'GoogleAuthCallbackResource'
            ),
        )
        try:
            auth_flow.fetch_token(
                authorization_response=get_api_base_url() + self.request.full_path
            )
        except errors.OAuth2Error as e:
            raise BadRequestParams(message=str(e))

        credentials = auth_flow.credentials
        access_token = credentials.token
        res = requests.get(
            url='https://www.googleapis.com/oauth2/v1/userinfo',
            params=dict(
                alt='json',
                access_token=access_token
            )
        )

        if res.status_code != 200:
            raise ServerError(message=res.text)
        try:
            user_info = res.json()
        except Exception as e:
            raise ServerError(message=str(e))

        bl = BusinessLogic(self.session)

        ref_id = user_info['id']
        auth_info = self.session.query(ThirdPartyPlatform).filter(
            ThirdPartyPlatform.ref_id == ref_id,
            ThirdPartyPlatform.code == platform_id
        ).first()
        if auth_info:
            auth_info.meta = user_info
            account = auth_info.account
        else:
            auth_info = ThirdPartyPlatform(
                meta=user_info,
                code=platform_id,
                ref_id=ref_id,
            )
            account = bl.create_account(**self._map_account_data(user_info))
            auth_info.account = account

        self.session.add(auth_info)
        self.session.add(account)

        self.session.commit()

        auth, access_token = bl.authenticate(
            account=account
        )

        redirect_url = cache_data['callback_url']
        request_id = cache_data['request_id']

        self.session.commit()
        self.redis.delete(cache_key)

        params = {
            'access_token': access_token.value,
            'refresh_token': auth.id,
            'account_id': account.id,
            'request_id': request_id
        }

        return redirect(
            redirect_url + f'?{parse.urlencode(params)}'
        )

    @staticmethod
    def _map_account_data(user_info: dict) -> dict:
        return {

        }
