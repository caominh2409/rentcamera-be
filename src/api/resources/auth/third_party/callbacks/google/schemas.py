from src.bases.schema import (BaseSchema, IdField, StringField,
                              STRING_LENGTH_VALIDATORS)


class GetSchema(BaseSchema):
    state = StringField(
        required=True,
        validate=STRING_LENGTH_VALIDATORS['SHORT']
    )
