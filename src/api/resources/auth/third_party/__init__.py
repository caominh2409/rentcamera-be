from src.bases.api.resource import Resource


class ThirdPartyAuthResource(Resource):
    endpoint = '/auth/3rd-party'
