from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.core.auth.authentication import AuthenticationLogic
from src.bases.error.core import CoreError
# from src.models import Authentication
from src.common.utils import decode_jwt_token

from config import SECRET_KEY
# from src.models.account import AccountRole
# from src.models.permission import Permission
# from src.models.role import Role, RoleApp, RolePermission
# from src.models import App

from .schemas import PostSchema, GetSchema


class Post(MethodHandler):
    input_schema_class = PostSchema

    def handle_logic(self):
        email = self.payload['email']
        scope_type = self.payload['scope_type']
        secret_key = self.payload['secret_key']
        scope_id = self.payload.get('scope_id')

        logic = AuthenticationLogic(self.session)

        try:
            auth, access_token = logic.authenticate(
                email=email,
                scope_type=scope_type,
                scope_id=scope_id,
                secret_key=secret_key
            )
        except CoreError as e:
            if e.error in [
                'InvalidCredentials',
                'MissingScopeId',
                'InvalidScopeType'
            ]:
                raise BadRequestParams(e.error)
            raise ServerError(e.error)

        self.session.commit()

        if auth:
            account = auth.account
            # Get role by account
            account_roles = self.session.query(AccountRole).filter(
                    AccountRole.account_id == account.id
                ).all()
            role_ids = list(map(
                lambda x: x.role_id,
                account_roles
            ))
            roles = self.session.query(Role).filter(Role.id.in_(role_ids))
            roles_list =  [r.to_json() for r in roles]

            #Role permissions, role apps
            list_role_permissions = []
            list_role_apps = []

            for role in roles:
                # Get permission by role
                list_permission = self._mapping_permissions(role.id)
                list_role_permissions.extend(list_permission)

                # Get app by role
                list_app = self._mapping_apps(role.id)
                list_role_apps.extend(list_app)

            # Returns an array without duplicate values
            list_role_apps = list({v['id']:v for v in list_role_apps}.values())
            list_role_permissions = list({v['id']:v for v in list_role_permissions}.values())

        return dict(
            access_token=access_token.value,
            refresh_token=auth.id,
            scope_type=auth.scope_type,
            scope_id=auth.scope_id,
            account=auth.account.to_json(),
            roles=roles_list,
            permissions=list_role_permissions,
            apps=list_role_apps
        )

    def _mapping_permissions(self, role_id):
        permissions_list = []
        role_permissions = self.session.query(RolePermission).filter(
                        RolePermission.role_id == role_id
                    ).all()
        permission_ids = list(map(
            lambda x: x.permission_id,
            role_permissions
        ))
        permissions = self.session.query(Permission).filter(Permission.id.in_(permission_ids))
        permissions_list =  [r.to_json() for r in permissions]

        return permissions_list

    def _mapping_apps(self, role_id):
        apps_list = []
        role_apps = self.session.query(RoleApp).filter(
                        RoleApp.role_id == role_id
                    ).all()
        app_ids = list(map(
            lambda x: x.app_id,
            role_apps
        ))
        apps = self.session.query(App).filter(App.id.in_(app_ids))
        apps_list =  [r.to_json() for r in apps]

        return apps_list

class Get(MethodHandler):
    input_schema_class = GetSchema

    def handle_logic(self):
        access_token = self.payload['access_token']

        token_data = decode_jwt_token(token=access_token,
                                      secret_key=SECRET_KEY,
                                      verify_exp=True)
        if not token_data:
            return None

        try:
            refresh_token = token_data['refresh_token']
            auth = self.session.query(
                Authentication
            ).get(refresh_token)

        except Exception as e:
            raise BadRequestParams('InvalidToken', str(e))

        return dict(
            access_token=access_token,
            refresh_token=auth.id,
            scope_type=auth.scope_type,
            scope_id=auth.scope_id,
            account=auth.account.to_json()
        )
