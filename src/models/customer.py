from sqlalchemy import Column, String

from src.common.constants import STRING_LENGTH
from src.bases.model import BaseModel


class Customer(BaseModel):
    platform = Column(String(STRING_LENGTH['EX_SHORT']),
                      nullable=False, index=True)
    uid = Column(String(STRING_LENGTH['LONG']),
                 nullable=False, index=True)
    username = Column(String(STRING_LENGTH['LONG']),
                      nullable=False, index=True)
    first_name = Column(String(STRING_LENGTH['LONG']),
                        nullable=False, index=True)
    last_name = Column(String(STRING_LENGTH['LONG']),
                       nullable=False, index=True)
    gender = Column(String(STRING_LENGTH['EX_SHORT']),
                    nullable=False, index=True)
    phone = Column(String(STRING_LENGTH['EX_SHORT']),
                   nullable=False, index=True)
    title = Column(String(STRING_LENGTH['EX_SHORT']),
                   nullable=False, index=True)
