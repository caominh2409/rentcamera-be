from sqlalchemy import (Integer, ForeignKey, Column, String,
                        Float, JSON, Boolean)
from sqlalchemy.orm import relationship, backref

from src.common.constants import STRING_LENGTH
from src.bases.model import BaseModel

from config import APP_BASE_DOMAIN, ENVIRONMENT


class App(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True)
    code = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True, unique=True)
    secret_key = Column(String(STRING_LENGTH['LONG']),
                        nullable=False, index=True, unique=True)
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    nullable=False, index=True)
    version_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('app_version.id'), index=True)

    number = Column(Integer, index=True, nullable=False)

    general_settings = relationship('AppGeneralSettings',
                                    backref=backref('app'),
                                    uselist=False)
    datetime_settings = relationship('AppDatetimeSettings',
                                     backref=backref('app'),
                                     uselist=False)
    currency_settings = relationship('AppCurrencySettings',
                                     backref=backref('app'),
                                     uselist=False)
    booking_settings = relationship('AppBookingSettings',
                                    backref=backref('app'),
                                    uselist=False)
    notification_settings = relationship('AppNotificationSettings',
                                         backref=backref('app'),
                                         uselist=False)
    cms_settings = relationship('AppCmsSettings',
                                backref=backref('app'),
                                uselist=False)
    age_policy = relationship('AppAgePolicy',
                              backref=backref('app'),
                              uselist=False)
    version = relationship('AppVersion',
                           backref=backref('apps'))

    @property
    def default_domain(self):
        result = '{prefix}.{base_domain}'.format(
            prefix=self.code,
            base_domain=APP_BASE_DOMAIN
        )
        if ENVIRONMENT != 'production':
            result = f'{ENVIRONMENT}-{result}'
        return result

    @property
    def base_url(self):
        return f'https://{self.default_domain}'

    @property
    def release_name(self):
        return f'{ENVIRONMENT}-{self.code}'

    def to_json(self, *args, **kwargs):
        result = super(App, self).to_json(*args, **kwargs)

        result['version'] = self.version.to_json()

        return result


class AppMember(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    account_id = Column(String(STRING_LENGTH['UUID4']),
                        index=True, nullable=False)

    account = Column(JSON, nullable=False)

    app = relationship('App',
                       backref=backref('members',
                                       cascade='delete,all'))


class AppGeneralSettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    cms_url = Column(String(STRING_LENGTH['LARGE']))

    b2b_enabled = Column(Boolean, default=False)
    postpaid_enabled = Column(Boolean, default=False)
    airasia_login = Column(Boolean, default=False)
    client_topup = Column(Boolean, default=False)


class AppCmsSettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    head_injection = Column(String(STRING_LENGTH['EX_LARGE']))
    body_injection = Column(String(STRING_LENGTH['EX_LARGE']))
    custom_css = Column(String(STRING_LENGTH['EX_LARGE']))

    logo = Column(JSON)
    favicon = Column(JSON)


class AppBookingSettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    code_prefix = Column(String(STRING_LENGTH['EX_SHORT']),
                         nullable=False, unique=True, index=True)

    start_number = Column(Integer, default=0)

    wfpf_value = Column(Float, default=0.0)


class AppDatetimeSettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    date_format = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False)
    time_format = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False)


class AppNotificationSettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    booking_smtp_id = Column(String(STRING_LENGTH['UUID4']),
                             ForeignKey('smtp.id'),
                             index=True)
    booking_success_template_id = Column(String(STRING_LENGTH['UUID4']),
                                         ForeignKey('html_template.id'),
                                         index=False)
    booking_in_progress_template_id = Column(String(STRING_LENGTH['UUID4']),
                                             ForeignKey('html_template.id'),
                                             index=False)
    booking_cancel_template_id = Column(String(STRING_LENGTH['UUID4']),
                                        ForeignKey('html_template.id'),
                                        index=False)

    booking_success_subject = Column(String(STRING_LENGTH['LARGE']))
    booking_cancel_subject = Column(String(STRING_LENGTH['LARGE']))
    booking_in_progress_subject = Column(String(STRING_LENGTH['LARGE']))

    booking_success_cc = Column(JSON, default=[])
    booking_cancel_cc = Column(JSON, default=[])
    booking_in_progress_cc = Column(JSON, default=[])

    booking_success_bcc = Column(JSON, default=[])
    booking_cancel_bcc = Column(JSON, default=[])
    booking_in_progress_bcc = Column(JSON, default=[])


class AppAgePolicy(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)

    child_age = Column(Integer, default=12)
    infant_age = Column(Integer, default=2)


class AppCurrencySettings(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    index=True, nullable=False)
    conversion_update_frequency = Column(String(STRING_LENGTH['EX_SHORT']),
                                         index=True, default='daily')

    conversion_auto_update = Column(Boolean, default=True)


class AppContact(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    nullable=False, index=True)
    website = Column(String(STRING_LENGTH['SHORT']))
    facebook = Column(String(STRING_LENGTH['SHORT']))
    twitter = Column(String(STRING_LENGTH['SHORT']))
    whatsapp = Column(String(STRING_LENGTH['SHORT']))

    email = Column(String(STRING_LENGTH['LONG']), index=True)
    hotline = Column(String(STRING_LENGTH['LONG']))
    phone = Column(String(STRING_LENGTH['LONG']))

    app = relationship('App',
                       backref=backref('contact',
                                       cascade='delete,all',
                                       uselist=False))


class AppVersion(BaseModel):
    code = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True, unique=True)
    frontend_version = Column(String(STRING_LENGTH['LONG']),
                              index=True, nullable=False)
    backend_version = Column(String(STRING_LENGTH['LONG']),
                             index=True, nullable=False)
    description = Column(String(STRING_LENGTH['EX_LARGE']))

    meta = Column(JSON)


class AppCustomDomain(BaseModel):
    app_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('app.id'),
                    nullable=False, index=True)

    value = Column(String(STRING_LENGTH['LONG']),
                   nullable=False,
                   index=True, unique=True)

    enabled = Column(Boolean, default=True, index=True)

    app = relationship('App',
                       backref=backref('domains',
                                       cascade='delete,all'))
