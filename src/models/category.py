from email.policy import default
from sqlalchemy import (Integer, ForeignKey, Column, String,
                        Float, JSON, Boolean)
from sqlalchemy.orm import relationship, backref

from src.common.constants import (STRING_LENGTH)
from src.bases.model import BaseModel


class Category(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True)
    label = Column(String(STRING_LENGTH['MEDIUM']),
                  nullable=False,
                  index=True)
    parent_id = Column(String(STRING_LENGTH['MEDIUM']), default='')

    def to_json(self,
                *args,
                **kwargs):
        result = super().to_json(*args, **kwargs)

        return result
