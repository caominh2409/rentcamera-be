from sqlalchemy import (Column, String, ForeignKey,
                        JSON, Table)
from sqlalchemy.orm import relationship, backref

from src.common.constants import STRING_LENGTH
from src.common.utils import hash_string
from src.bases.model import BaseModel

class Account(BaseModel):
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    default='active', index=True)
    type = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True)
    email = Column(String(STRING_LENGTH['LONG']),
                   index=True, nullable=False, unique=True)
    phone = Column(String(STRING_LENGTH['EX_SHORT']),
                   index=True)
    first_name = Column(String(STRING_LENGTH['SHORT']),
                        index=True)
    last_name = Column(String(STRING_LENGTH['SHORT']),
                       index=True)
    secret_key = Column(String(STRING_LENGTH['LONG']),
                        nullable=False)

    avatar = Column(JSON)

    def encrypt_secret_key(self, value: str) -> str:
        if not self.email:
            raise Exception('MissingEmail')

        return hash_string(value + self.email)

    def validate_secret_key(self, value: str) -> bool:
        return hash_string(value + self.email) == self.secret_key

    def to_json(self,
                *args,
                **kwargs):
        result = super(Account, self).to_json(*args, **kwargs)

        result.pop('secret_key', None)

        return result


class AccountRole(BaseModel):
    account_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('account.id'),
                        nullable=False, index=True)

    role_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('role.id'),
                      index=True, nullable=False)

class Role(BaseModel):
    type = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True)