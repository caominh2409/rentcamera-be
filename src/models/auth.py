from sqlalchemy import (Integer, ForeignKey, Column, String,
                        DateTime, JSON)
from sqlalchemy.orm import relationship, backref

from src.common.constants import STRING_LENGTH
from src.common.utils import get_now
from src.bases.model import BaseModel


class Authentication(BaseModel):
    account_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('account.id'),
                        nullable=False, index=True)
    scope_type = Column(String(STRING_LENGTH['EX_SHORT']),
                        nullable=False, index=True)
    scope_id = Column(String(STRING_LENGTH['UUID4']), index=True)

    expired_at = Column(DateTime(timezone=True),
                        nullable=False, index=True)

    account = relationship('Account', backref=backref('auths',
                                                      cascade='all,delete'))

    @property
    def is_expired(self) -> bool:
        if not self.expired_at:
            return True
        return self.expired_at <= get_now()


class AccessToken(BaseModel):
    auth_id = Column(String(STRING_LENGTH['UUID4']),
                     ForeignKey('authentication.id'),
                     nullable=False, index=True)
    account_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('account.id'),
                        nullable=False, index=True)
    value = Column(String(STRING_LENGTH['MEDIUM']),
                   nullable=False, index=True)

    expired_at = Column(DateTime(timezone=True),
                        nullable=False, index=True)

    account = relationship('Account',
                           backref=backref('access_tokens',
                                           cascade='all,delete'))
    auth = relationship('Authentication',
                        backref=backref('access_tokens',
                                        cascade='all,delete'))
