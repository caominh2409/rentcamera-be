from sqlalchemy import (Column, String)

from src.common.constants import STRING_LENGTH
from src.bases.model import BaseModel


class DateFormat(BaseModel):
    code = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True, unique=True)
    js_moment = Column(String(STRING_LENGTH['EX_SHORT']),
                       nullable=False)
    js_element = Column(String(STRING_LENGTH['EX_SHORT']),
                        nullable=False)
    py = Column(String(STRING_LENGTH['EX_SHORT']),
                nullable=False)
    example = Column(String(STRING_LENGTH['EX_SHORT']),
                     nullable=False)


class TimeFormat(BaseModel):
    code = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True, unique=True)
    js_moment = Column(String(STRING_LENGTH['EX_SHORT']),
                       nullable=False)
    js_element = Column(String(STRING_LENGTH['EX_SHORT']),
                        nullable=False)
    py = Column(String(STRING_LENGTH['EX_SHORT']),
                nullable=False)
    example = Column(String(STRING_LENGTH['EX_SHORT']),
                     nullable=False)
