from sqlalchemy import Column, String

from src.common.constants import STRING_LENGTH
from src.bases.model import BaseModel


class DockerImage(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True)
    version = Column(String(STRING_LENGTH['LONG']),
                     nullable=False, index=True)
    type = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True)

