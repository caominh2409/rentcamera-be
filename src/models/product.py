from sqlalchemy import (Integer, ForeignKey, Column, String,
                        Float, JSON, Boolean)
from sqlalchemy.orm import relationship, backref

from src.common.constants import (STRING_LENGTH)
from src.bases.model import BaseModel


class Product(BaseModel):
    name = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False, index=True)
    code = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False,
                  index=True)
    img = Column(String(STRING_LENGTH['LONG']))
    bio = Column(String(STRING_LENGTH['LONG']), nullable=True)
    description = Column(String(STRING_LENGTH['LARGE']), nullable=True)
    # ingredients = Column(String(STRING_LENGTH['LONG']), nullable=True)
    meta = Column(JSON, default=[])
    brand = Column(String(STRING_LENGTH['EX_SHORT']),
                  nullable=False,
                  index=True)
    price = Column(Integer)

    def to_json(self,
                *args,
                **kwargs):
        result = super().to_json(*args, **kwargs)

        return result

class ProductCategory(BaseModel):
    product_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('product.id'),
                        nullable=False, index=True)

    category_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('category.id'),
                      index=True, nullable=False)