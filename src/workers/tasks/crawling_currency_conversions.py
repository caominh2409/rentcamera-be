from src.core.currency import CurrencyLogic
from src.bases.celery.task import Task, TaskHandler


class Handler(TaskHandler):
    def run(self,
            scope_type: str,
            scope_id: str = None,
            currency_codes: list = None):
        bl = CurrencyLogic(session=self.session)

        updating_conversions = bl.update_conversions(
            scope_type=scope_type,
            scope_id=scope_id,
            currency_codes=currency_codes
        )

        chunk_size = 5000
        count = 0
        for conversion in updating_conversions:
            count += 1
            self.session.add(conversion)
            if count >= chunk_size:
                self.session.commit()
                count = 0
        self.session.commit()


class CrawlingCurrencyConversions(Task):
    name = 'CrawlingCurrencyConversions'
    handler = Handler
