from src.bases.celery.task import Task, TaskHandler
from src.common.utils import log


class Handler(TaskHandler):
    def run(self,
            app_id: str,
            sync_type: str,
            payload: dict):
        pass
        # app = self.session.query(App).get(app_id)
        # if not app:
        #     raise Exception('AppNotFound')

        # sync_logic = AppSyncLogic(app=app,
        #                           session=self.session,
        #                           mongo=self.mongo)

        # try:
        #     if sync_type == 'all':
        #         excludes = payload.get('excludes')
        #         includes = payload.get('includes')
        #         update_check_models = payload.get('update_check_models')
        #         create_indexes = payload.get('create_indexes')
        #         sync_logic.sync_all(excludes=excludes,
        #                             includes=includes,
        #                             create_indexes=create_indexes,
        #                             update_check_models=update_check_models)

        #     else:
        #         for model_name, data in payload.items():
        #             sync_logic.sync(
        #                 model_name=model_name,
        #                 commands=data['commands'],
        #                 create_indexes=data.get('create_indexes')
        #             )

        # except Exception as e:
        #     log.exception(e)


class SyncingApp(Task):
    name = 'SyncingApp'
    handler = Handler
