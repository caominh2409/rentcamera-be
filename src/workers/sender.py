from src.bases.celery.generator import CeleryWorkerGenerator

from config import CeleryConfig

generator = CeleryWorkerGenerator(
    name='CoreEngineTaskSender',
    config=CeleryConfig,
)

sender = generator.run()
