from src.models import CmsTemplate, CmsNode, CmsNodeType, ShoppingForm, App
from src.databases import Postgres

from config import OLD_POSTGRES_URI

from .base import Migrator


class MigratingCms(Migrator):

    def run(self):
        old_db = Postgres(OLD_POSTGRES_URI)

        for cnt in old_db.engine.execute(
            f"SELECT * FROM cms_node_type"
        ):
            node_type = self.session.query(CmsNodeType).get(cnt.id)
            if not node_type:
                node_type = CmsNodeType()
                node_type.id = cnt.id

            if cnt.app_env_id:
                continue

            node_type.scope_type = 'System'
            node_type.parent_id = cnt.parent_id
            node_type.category = cnt.category
            node_type.name = cnt.name
            node_type.icon = cnt.icon
            node_type.code = cnt.code
            node_type.has_children = cnt.has_children
            node_type.classes = cnt.classes
            node_type.props = cnt.props
            node_type.styles = cnt.styles
            self.session.add(node_type)

        for ct in old_db.engine.execute(
            f"SELECT * FROM cms_template"
        ):

            cms_template = self.session.query(CmsTemplate).get(ct.id)
            if not cms_template:
                cms_template = CmsTemplate()
                cms_template.id = ct.id

            if ct.app_env_id:
                continue

            cms_template.scope_type = 'System'
            cms_template.name = ct.name

            for cn in old_db.engine.execute(
                f"SELECT * FROM cms_node WHERE template_id = '{cms_template.id}'"
            ):
                cms_node = self.session.query(CmsNode).get(cn.id)
                if not cms_node:
                    cms_node = CmsNode()
                    cms_node.id = cn.id

                cms_node.type_id = cn.type_id
                cms_node.template_id = cms_template.id
                cms_node.parent_id = cn.parent_id
                cms_node.parent_ids = cn.parent_ids
                cms_node.order = cn.order
                cms_node.has_children = cn.has_children
                cms_node.classes = cn.classes
                cms_node.props = cn.props
                cms_node.styles = cn.styles
                self.session.add(cms_node)

            self.session.add(cms_template)

        self.session.commit()
