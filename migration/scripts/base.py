from sqlalchemy import orm


class Migrator(object):
    def __init__(self, engine):
        self.session = orm.Session(bind=engine, autoflush=False)

    def run(self):
        raise NotImplementedError
