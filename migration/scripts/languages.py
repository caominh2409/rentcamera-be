from src.models import Language
from src.databases import Postgres

from config import OLD_POSTGRES_URI

from .base import Migrator


class MigratingLanguages(Migrator):

    def run(self):
        old_db = Postgres(OLD_POSTGRES_URI)

        for l in old_db.engine.execute(
                "SELECT * FROM language"
        ):
            code = l['code']
            language = self.session.query(Language).filter(
                Language.code == code,
                Language.scope_type == 'System'
            ).first()
            if not language:
                language = Language()
                language.code = code
                language.scope_type = 'System'
            language.id = l['id']
            language.name = l['name']
            language.avatar = l.avatar
            language.right_to_left = l.right_to_left or False
            language.data = l.data
            self.session.add(language)

        self.session.commit()
