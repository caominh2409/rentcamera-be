from src.models import (Product, TravelerField, Provider,
                        DateFormat, TimeFormat, Supplier,
                        HelmChart, AppVersion)

from config import ENVIRONMENT

from .base import Migrator


class InitializingDb(Migrator):
    APP_VERSIONS = [
        {
            'code': '1.0.0',
            'backend_version': f'{ENVIRONMENT}_1.0.0',
            'frontend_version': f'{ENVIRONMENT}_1.0.0',
        }
    ]

    HELMCHARTS = [
        {
            'name': 'app',
            'version': '1.0.0'
        }
    ]

    DATE_FORMATS = [
        dict(
            code='dd/mm/yyyy',
            js_moment='DD/MM/YYYY',
            js_element='dd/MM/yyyy',
            py='%d/%m/%Y',
            example='30/12/2020'
        ),
        dict(
            code='yyyy/mm/dd',
            js_moment='YYYY/MM/DD',
            js_element='yyyy/MM/dd',
            py='%Y/%m/%d',
            example='2020/12/30'
        ),
        dict(
            code='mm/dd/yyyy',
            js_moment='MM/DD/YYYY',
            js_element='MM/dd/yyyy',
            py='%m/%d/%Y',
            example='01/20/2020'
        )
    ]

    TIME_FORMATS = [
        dict(
            code='hh:mm',
            js_moment='HH:mm',
            js_element='HH:mm',
            py='%H:%M',
            example='16:50'
        ),
        dict(
            code='hh:mm A',
            js_moment='hh:mm A',
            js_element='hh:mm A',
            py='%I:%M %p',
            example='07:50 PM'
        )
    ]

    PRODUCTS = [
        dict(code='hotel', icon='hotel', name='Hotel'),
        dict(code='flight', icon='flight', name='Flight'),
        dict(code='tour', icon='tour', name='Tour'),
        dict(code='tour_package', icon='backpack', name='Holiday Package'),
        dict(code='car_rental', icon='car_rental', name='Car Rental'),
        dict(code='transfer', icon='transfer_within_a_station',
             name='Airport Transfer'),
    ]

    TRAVELER_FIELDS = [
        dict(key='title', icon=None, data_type='str', name='Title'),
        dict(key='type', icon=None, data_type='str', name='Type'),
        dict(key='first_name', icon=None, data_type='str',
             name='First Name'),
        dict(key='last_name', icon=None, data_type='str', name='Last Name'),
        dict(key='birthday', icon=None, data_type='datetime', name='Birthday'),
        dict(key='phone', icon=None, data_type='str', name='Phone'),
        dict(key='country', icon=None,
             data_type='str', name='Nationality'),
        dict(key='passport_number', icon=None, data_type='str',
             name='Passport Number'),
        dict(key='passport_expire', icon=None, data_type='datetime',
             name='Passport Expire'),
        dict(key='passport_country', icon=None, data_type='str',
             name='Passport Country'),
    ]

    PROVIDERS = {
        'flight': [
            dict(name='AmadeusOTA', code='1A'),
            dict(name='AeroCRS', code='AeroCRS'),
            dict(name='Amadeus', code='Amadeus'),
            dict(name='Apollo', code='Apollo'),
            dict(name='FlyDubai', code='FlyDubai'),
            dict(name='FR24', code='FR24'),
            dict(name='IBS', code='IBS'),
            dict(name='ISA', code='ISA'),
            dict(name='Datacom', code='Datacom'),
            dict(name='Kiwi', code='Kiwi'),
            dict(name='Letsfly', code='Letsfly'),
            dict(name='MRD', code='MRD'),
            dict(name='Mystifly', code='Mystifly'),
            dict(name='Navitaire', code='Navitaire'),
            dict(name='PKFare', code='PKFare'),
            dict(name='Pyton', code='Pyton'),
            dict(name='Radixx', code='Radixx'),
            dict(name='RadixxGalaxy', code='RadixxGalaxy'),
            dict(name='Sabre', code='Sabre'),
            dict(name='SabreGDS', code='SabreGDS'),
            dict(name='Travelport', code='Travelport'),
            dict(name='TravelSky', code='TravelSky'),
            dict(name='ClarityTTS', code='ClarityTTS'),
            dict(name='Via', code='Via'),
        ],
        'hotel': [
            {
                'name': 'Agoda (B2C Comm)',
                'code': 'agymbwykbqc',
            },
            {
                'name': 'APItude Package',
                'code': 'apyacwgpawq',
            },
            {
                'name': 'HMS',
                'code': 'hm4odqcgdqm',
            },
            {
                'name': 'Juniper TUI',
                'code': 'jpskcg8mdq8',
            },
            {
                'name': 'NTA',
                'code': 'nt8acaqodqa',
            },
            {
                'name': 'PriceLine',
                'code': 'plilbgeobwc',
            },
            {
                'name': 'Smyrooms',
                'code': 'smrigkegcg',
            },
            {
                'name': 'RoomsXML',
                'code': 'rwoedqopxml',
            },
            {
                'name': 'Innstant',
                'code': 'innsawevxll',
            },
            {
                'name': 'Darina',
                'code': 'drnbksh7rh',
            },
            {
                'name': 'Traveloka Package',
                'code': 'tvlkejebgmc',
            },
            {
                'name': 'TBO USD',
                'code': 'tbodqisflas',
            },
            {
                'name': 'HMS HIT',
                'code': 'hm4odqcgdqm',
            },
            {
                'name': 'Getaroom',
                'code': 'getoiujiknl',
            },
            {
                'name': 'Quantum USD',
                'code': 'qtgcdgaacq8',
            },
            {
                'name': 'Jalan NTA',
                'code': 'jappeldaqwk',
            },
            {
                'name': 'EPS Package',
                'code': 'epsbkjh9ro',
            },
            {
                'name': 'APPS Rotana',
                'code': 'rot9xmwkve2',
            },
            {
                'name': 'APPS Marriot',
                'code': 'marvkjqj4kj',
            },
            {
                'name': 'APPS Infinite',
                'code': 'accnbus9253',
            },
            {
                'name': 'EYH Ehub',
                'code': 'ehbvdqg116u',
            },
            {
                'name': 'Anixe GoQuo',
                'code': 'ane1djqq898',
            },
            {
                'name': 'GIATA',
                'code': '6r71m3wm3x6',
            }
        ]
    }

    def _init_traveler_fields(self):
        for tf in self.TRAVELER_FIELDS:
            key = tf['key']
            field = self.session.query(TravelerField).filter(
                TravelerField.key == key,
                TravelerField.scope_type == 'System'
            ).first()
            if not field:
                field = TravelerField()
                field.key = key
                field.scope_type = 'System'
            field.icon = tf['icon']
            field.data_type = tf['data_type']
            field.translation_label = tf['name']
            self.session.add(field)
        self.session.commit()

    def _init_products(self):
        for p in self.PRODUCTS:
            product_code = p['code']
            product = self.session.query(Product).filter(
                Product.code == product_code,
                Product.scope_type == 'System'
            ).first()
            if not product:
                product = Product()
                product.code = product_code
                product.scope_type = 'System'
            product.name = p['name']
            product.icon = p['icon']
            self.session.add(product)
        self.session.commit()

    def _init_datetime_formats(self):
        for f in self.DATE_FORMATS:
            code = f['code']
            date_format = self.session.query(DateFormat).filter(
                DateFormat.code == code
            ).first()
            if not date_format:
                date_format = DateFormat()
                date_format.code = code
            date_format.update(**f)
            self.session.add(date_format)

        for f in self.TIME_FORMATS:
            code = f['code']
            time_format = self.session.query(TimeFormat).filter(
                TimeFormat.code == code
            ).first()
            if not time_format:
                time_format = TimeFormat()
                time_format.code = code
            time_format.update(**f)
            self.session.add(time_format)

        self.session.commit()

    def _init_providers(self):
        for product_code, providers in self.PROVIDERS.items():
            for p in providers:
                code = p['code']
                provider = self.session.query(Provider).filter(
                    Provider.code == code,
                    Provider.product_code == product_code
                ).first()
                if not provider:
                    provider = Provider()
                    provider.code = code
                    provider.product_code = product_code
                provider.name = p['name']
                self.session.add(provider)
        self.session.commit()

    def _init_app_versions(self):
        for av in self.APP_VERSIONS:
            code = av['code']
            version = self.session.query(AppVersion).filter(
                AppVersion.code == code
            ).first()
            if not version:
                version = AppVersion()
                version.code = code
            version.backend_version = av['backend_version']
            version.frontend_version = av['frontend_version']
            self.session.add(version)
        self.session.commit()

    def _init_helmcharts(self):
        for hc in self.HELMCHARTS:
            version = hc['version']
            name = hc['name']
            helm_chart = self.session.query(HelmChart).filter(
                HelmChart.version == version,
                HelmChart.name == name
            ).first()
            if not helm_chart:
                helm_chart = HelmChart()
                helm_chart.version = version
                helm_chart.name = name
            self.session.add(helm_chart)
        self.session.commit()

    def run(self):
        self._init_app_versions()
        self._init_helmcharts()
        self._init_products()
        self._init_traveler_fields()
        self._init_datetime_formats()
        self._init_providers()
