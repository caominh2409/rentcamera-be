from src.models import App, TravelerField
from src.core.app import AppLogic

from .base import Migrator

APPS = [
    {
        'code': 'aa',
        'name': 'AirAsia',
        'settings': {
            'general_settings': {
                'postpaid_enabled': True
            },
            'cms_settings': {},
            'booking_settings': {
                'code_prefix': 'AA'
            },
            'datetime_settings': {
                'date_format': 'dd/mm/yyyy',
                'time_format': 'hh:mm'
            },
            'age_policy': {},
            'currency_settings': {}
        },
        'products': [
            {
                'code': 'flight'
            }
        ],
    },
    {
        'code': 'aahol',
        'name': 'Holiday',
        'settings': {
            'general_settings': {
                'postpaid_enabled': True
            },
            'cms_settings': {},
            'booking_settings': {
                'code_prefix': 'HLD'
            },
            'datetime_settings': {
                'date_format': 'dd/mm/yyyy',
                'time_format': 'hh:mm'
            },
            'age_policy': {},
            'currency_settings': {}
        },
        'products': [
            {
                'code': 'flight'
            },
            {
                'code': 'hotel'
            },
            {
                'code': 'tour'
            }
        ]
    },
]


class MigratingApps(Migrator):
    def run(self):
        app_logic = AppLogic(self.session)

        for a in APPS:
            code = a['code']
            if self.session.query(App).filter(
                    App.code == code
            ).first():
                continue

            traveler_fields = []

            for tf in self.session.query(TravelerField).filter(
                    TravelerField.scope_type == 'System'
            ):
                traveler_fields.append({
                    'key': tf.key,
                    'conditions': [
                        {
                            'product_code': 'tour',
                            'enabled': True,
                            'route_type': 'all'
                        },
                        {
                            'product_code': 'car_rental',
                            'enabled': True,
                            'route_type': 'all'
                        },
                        {
                            'product_code': 'tour_package',
                            'enabled': True,
                            'route_type': 'all'
                        },
                        {
                            'product_code': 'hotel',
                            'enabled': True,
                            'route_type': 'all'
                        },
                        {
                            'product_code': 'flight',
                            'enabled': True,
                            'route_type': 'all'
                        },
                        {
                            'product_code': 'transfer',
                            'enabled': True,
                            'route_type': 'all'
                        }
                    ]
                })

            app_logic.create(
                name=a['name'],
                code=code,
                settings=a['settings'],
                products=a['products'],
                traveler_fields=traveler_fields
            )
        self.session.commit()
