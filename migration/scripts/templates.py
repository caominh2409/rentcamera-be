from src.models import App
from src.core.app import AppLogic

from .base import Migrator


class MigratingTemplates(Migrator):
    def run(self):
        app_logic = AppLogic(self.session)
        for app in self.session.query(App):
            app_logic.create_default_html_templates(app)
        self.session.commit()
