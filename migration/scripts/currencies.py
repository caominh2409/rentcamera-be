from src.models import Currency, CurrencyConversion
from src.databases import Postgres

from config import OLD_POSTGRES_URI

from .base import Migrator


class MigratingCurrencies(Migrator):

    def run(self):
        old_db = Postgres(OLD_POSTGRES_URI)

        for c in old_db.engine.execute(
                "SELECT * FROM currency"
        ):
            code = c.code
            currency = self.session.query(Currency).filter(
                Currency.code == code,
                Currency.scope_type == 'System'
            ).first()
            if not currency:
                currency = Currency()
                currency.code = code
                currency.scope_type = 'System'
            currency.id = c.id
            currency.name = c.name
            currency.symbol = c.symbol
            currency.rounding = c.rounding
            currency.country_name = c.country_name
            self.session.add(currency)

        for cc in old_db.engine.execute(
            "SELECT * FROM currency_conversion WHERE app_env_id IS NULL"
        ):
            from_code = cc.from_cur_code
            to_code = cc.to_cur_code
            conversion = self.session.query(CurrencyConversion).filter(
                CurrencyConversion.scope_type == 'System',
                CurrencyConversion.from_code == from_code,
                CurrencyConversion.to_code == to_code
            ).first()
            if not conversion:
                conversion = CurrencyConversion()
                conversion.from_code = from_code
                conversion.to_code = to_code
                conversion.scope_type = 'System'
            conversion.rate = cc.rate
            self.session.add(conversion)

        self.session.commit()
