from src.models import Supplier, Product
from src.databases import Postgres

from config import OLD_POSTGRES_URI

from .base import Migrator


class MigratingSuppliers(Migrator):

    def run(self):
        old_db = Postgres(OLD_POSTGRES_URI)

        for gw in old_db.engine.execute(
                "SELECT * FROM gateway"
        ):
            gw_id = gw['id']
            gw_type = gw['type']

            supplier = self.session.query(Supplier).get(gw_id)
            if not supplier:
                supplier = Supplier()
                supplier.id = gw_id
            supplier.name = gw['name']
            supplier.code = 'goquo'
            supplier.type = gw_type
            supplier.urls = gw['urls']

            env = old_db.engine.execute(
                f"SELECT * FROM env WHERE id = '{gw.env_id}'"
            ).first()
            supplier.env = 'staging' if env.code == 'stag' else 'production'

            product_id = gw.product_id
            if gw_type == 'product':
                if not product_id:
                    continue
                p = old_db.engine.execute(
                    f"SELECT * FROM product WHERE id = '{product_id}'"
                ).first()
                if not p:
                    continue

                product = self.session.query(Product).filter(
                    Product.code == p['code']
                ).first()
                if not product:
                    continue
                supplier.product_code = product.code

            self.session.add(supplier)

        self.session.commit()
