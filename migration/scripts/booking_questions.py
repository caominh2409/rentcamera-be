from src.models import (
    BookingQuestion, App, Product, BookingTravelerDetail,
    TravelerField
)
from src.core.app import AppLogic

from .base import Migrator


class MigratingBookingQuestions(Migrator):
    QUESTIONS = [
        dict(
            key='title',
            data_type='str',
            input_type='select',
            importance='mandatory',
            group='travelers',
            translation_label='Title',
            meta=dict(
                options=['Mr', 'Ms', 'Mrs']
            ),
            editable=False,
        ),
        dict(
            key='type',
            data_type='str',
            input_type='select',
            importance='mandatory',
            group='travelers',
            translation_label='Type',
            meta=dict(
                options=['adult', 'child', 'infant']
            ),
            editable=False,
        ),
        dict(
            key='first_name',
            data_type='str',
            input_type='textinput',
            importance='mandatory',
            group='travelers',
            translation_label='First Name',
            editable=False,
        ),
        dict(
            key='last_name',
            data_type='str',
            input_type='textinput',
            importance='mandatory',
            group='travelers',
            translation_label='Last Name',
            editable=False,
        ),
        dict(
            key='birthday',
            data_type='date',
            input_type='date-picker',
            importance='mandatory',
            group='travelers',
            translation_label='Birthday'
        ),
        dict(
            key='phone',
            data_type='str',
            input_type='phoneinput',
            importance='mandatory',
            group='travelers',
            translation_label='Phone'
        ),
        dict(
            key='country',
            data_type='str',
            input_type='country-select',
            importance='mandatory',
            group='travelers',
            translation_label='Nationality'
        ),
        dict(
            key='passport_number',
            data_type='str',
            input_type='textinput',
            importance='mandatory',
            group='travelers',
            translation_label='Passport Number'
        ),
        dict(
            key='passport_expire',
            data_type='date',
            input_type='date-picker',
            importance='mandatory',
            group='travelers',
            translation_label='Passport Expire'
        ),
        dict(
            key='passport_country',
            data_type='str',
            input_type='country-select',
            importance='mandatory',
            group='travelers',
            translation_label='Passport Country'
        ),
    ]

    def run(self):
        products = self.session.query(Product).filter(
            Product.scope_type == 'System'
        ).all()
        for q in self.QUESTIONS:
            key = q['key']
            question = self.session.query(BookingQuestion).filter(
                BookingQuestion.key == key,
                BookingQuestion.scope_type == 'System'
            ).first()
            if not question:
                question = BookingQuestion()
                question.key = key
                question.scope_type = 'System'
            question.update(**q)
            question.deletable = False
            question.conditions = [{
                'product_code': product.code,
                'enabled': True,
                'route_type': 'all'
            } for product in products]

            self.session.add(question)

        self.session.commit()

        app_logic = AppLogic(self.session)

        for app in self.session.query(App):
            app_logic.init_booking_questions(app)

        self.session.commit()

        for btd in self.session.query(BookingTravelerDetail):
            field = self.session.query(TravelerField).get(btd.field_id)
            if not field:
                self.session.delete(btd)
                continue
            question = self.session.query(BookingQuestion).filter(
                BookingQuestion.scope_type == 'App',
                BookingQuestion.scope_id == btd.traveler.booking.app_id,
                BookingQuestion.key == field.key
            ).first()
            if not question:
                self.session.delete(btd)
                continue
            btd.data_type = question.data_type
            btd.key = question.key
            btd.translation_label = question.translation_label
            self.session.add(btd)
        self.session.commit()
