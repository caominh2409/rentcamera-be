## Setup database

$ sudo su postgres
$ psql
postgres=# create database rentalcamera encoding='UTF-8';
CREATE DATABASE
postgres=# create user rental with password '123456';
CREATE ROLE
postgres=# grant all privileges on database rentalcamera to rental;
GRANT
postgres=# \q

## Install library
pip install poetry
poetry install --no-interaction

## Migration db
poetry shell
export PYTHONPATH=${PWD}
alembic revision --autogenerate -m "comment here"
alembic upgrade head

## Run app
poetry shell
python3 run.py api