import yaml
import os

ROOT_PATH = os.path.dirname(__file__)

CONFIG_FILE_PATH = os.path.join(ROOT_PATH, 'env.yaml')

if os.path.exists(CONFIG_FILE_PATH):
    with open(CONFIG_FILE_PATH, 'r') as r_file:
        data = yaml.safe_load(r_file)
else:
    data = dict()

ENVIRONMENT = data.get('ENVIRONMENT', 'local')

SECRET_KEY = data.get('SECRET_KEY', 'secret_key')

MONGO_URI = data.get('MONGO_URI', 'mongo_uri')
POSTGRES_URI = data.get('POSTGRES_URI', 'postgres_uri')

APP_BASE_DOMAIN = data.get('APP_BASE_DOMAIN', 'apps.easygds.com')
BASE_DOMAIN = data.get('BASE_DOMAIN', 'localhost')

GCP_PROJECT = data.get('GCP_PROJECT')
GCP_K8S_CLUSTER = data.get('GCP_K8S_CLUSTER')
GCP_K8S_CLUSTER_ZONE = data.get('GCP_K8S_CLUSTER_ZONE')
IMAGE_REGISTRY = data.get('IMAGE_REGISTRY')
BACKEND_IMAGE_REPO = data.get('BACKEND_IMAGE_REPO')
WEBAPP_IMAGE_REPO = data.get('WEBAPP_IMAGE_REPO')
HELM_CHART_REPO = data.get('HELM_CHART_REPO')

AUTH_EXPIRE_TIME = data.get('AUTH_EXPIRE_TIME', 72 * 60 * 60)  # 72 hours
ACCESS_TOKEN_EXPIRE_TIME = data.get('ACCESS_TOKEN_EXPIRE_TIME', 1 * 60 * 60)

REDIS = data.get('REDIS', {
    'host': 'localhost',
    'port': 6379,
    'password': 'password'
})

DEFAULT_SMTP = data.get('DEFAULT_SMTP', {
    'password': 'Akfa+o2fYbBi2zAiFqL0JTBSqKcOdRgV+dKa8ExhADwS',
    'host': 'email-smtp.eu-west-1.amazonaws.com',
    'username': 'AKIAJYHWQMG4X4TEJNUQ',
    'port': 587,
    'email': 'noreply@easygds.com'
})

SERVICES = data.get('SERVICES', dict(
    IAM={
        'url': 'http://localhost:5001'
    },
    Storage={
        'url': 'https://storage.engine.easygds.com'
    }
))


class ApiConfig(object):
    ENV = ENVIRONMENT

    SECRET_KEY = SECRET_KEY

    DEBUG = ENVIRONMENT != 'production'

    MAX_CONTENT_LENGTH = 15 * 1024 * 1024

    PROPAGATE_EXCEPTIONS = True


class CeleryConfig(object):
    broker_url = 'redis://:{password}@{host}:{port}/{db}'.format(
        password=REDIS['password'],
        port=REDIS['port'],
        host=REDIS['host'],
        db=0
    )
